package july.day210701.test01;

import org.junit.Test;

/**
 * 测试输入法的半角 和全角
 * 
 * 全角输出的标点符号是 中文符号
 */
public class TestBanjiaoHeQuanjiao {
	
	@Test
	public void test01(){
		int a = 1;
		int b = 2;
		System.out.println(a+b);
		
		char c = 12345;
		System.out.println(c);
	}
}
