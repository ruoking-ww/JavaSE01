package july.day210701.test01;

import java.util.Arrays;

/**
 * 本类用来完成冒泡排序
 * 
 *  实现冒泡排序
	我们可以通过嵌套for循环来实现:
	外层循环来控制比较的轮数:最大轮数=个数-1
	内层循环来控制每轮比较的次数
	在比较过程中,如果顺序不对,就互换元素的位置
 */
public class TestBubbleSort {

	public static void main(String[] args) {
		int[] s = {100,22,20,50,7,3,4};
		int[] n = method02(s);
		System.out.println(Arrays.toString(n));
	}
	
	//把数组中的数据从小到大排序
	public static int[] method01(int[] a){
		//注意: 这里 i=1
		for(int i = 1; i<a.length; i++){
			System.out.println("第"+i+"轮:");
			//注意: 这里 j=0
			for(int j=0; j<a.length-i; j++){
				if(a[j] > a[j+1]){
					int temp = a[j];
					a[j] = a[j+1];
					a[j+1] = temp;
				}
			}
			System.out.println("第"+i+"轮的结果:"+Arrays.toString(a));
		}
		return a;
	}
	
	
	//优化方案,将数据从小到大排序
	public static int[] method02(int[] a){
		for(int i=1; i<a.length; i++){
			boolean flag = false;
			for(int j=0; j<a.length-i; j++){
				if(a[j]>a[j+1]){
					int temp=a[j];
					a[j]=a[j+1];
					a[j+1]=temp;
					flag=true;
				}
			}
			if(flag==false){
				return a;
			}
		}
		return a;
	}
	
	
	
}
