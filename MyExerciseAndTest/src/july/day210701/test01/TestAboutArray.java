package july.day210701.test01;

import java.util.Arrays;
import java.util.Random;

import org.junit.Test;

public class TestAboutArray {

/*
 * 练习: 遍历数组,存入1到10
 */
	@Test
	public void m2(){
		int[] a = new int[10];
		for(int i=0; i<a.length; i++){
			a[i] = i+1;
		}
		System.out.println(a);
		System.out.println(Arrays.toString(a));
	}
	
/*
 * 练习: 创建随机数组
 */
	@Test
	public void m3(){
		int[] a = new int[10];
		for(int i=0; i<a.length; i++){
			a[i] = new Random().nextInt(100);
		}
		System.out.println(Arrays.toString(a));
	}
}
































