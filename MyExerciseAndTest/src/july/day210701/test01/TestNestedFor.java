package july.day210701.test01;

import org.junit.Test;

/**
 * 测试嵌套for循环
 */
public class TestNestedFor {

	//打印右直角三角形
	@Test
	public void test01(){
		for(int i=1; i<=10; i++){
			for(int a=9; a>=i; a--){
				System.out.print("  ");
			}
			for(int j=1; j<=i; j++){
				System.out.print("* ");
			}
			System.out.println();
		}
	}
	
	//打印菱形
	@Test
	public void test04(){
		test02();
		test03();
	}
	
	//打印等边三角形的方法  //注意空格的区别 就可以了
	public void test02(){
		for(int i=1; i<=10; i++){
			for(int a=9; a>=i; a--){
				System.out.print(" ");
			}
			for(int j=1; j<=i; j++){
				System.out.print("* ");
			}
			System.out.println();
		}
	}
	//打印一个倒着的等边三角形的方法
	public void test03(){
		for(int i=1; i<=9; i++){
			for(int a=1; a<=i; a++){
				System.out.print(" ");
			}
			for(int j=9; j>=i; j--){
				System.out.print("* ");
			}
			System.out.println();
		}
	}
}



































