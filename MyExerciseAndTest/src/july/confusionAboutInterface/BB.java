package july.confusionAboutInterface;

public class BB<V> extends CC implements AA {
    @Override
    public void bb() {
        System.out.println("bb");
    }

    @Override
    public void aa() {
        System.out.println("aaaa");
    }

    //@Override
    //public V doSome(Object o) {
    //    System.out.println("哈哈哈");
    //    return (V) o;
    //}

}
