package july.confusionAboutInterface;

public interface AA<V> {

    void bb();

    default void aa(){
        System.out.println("aa");
    }

    default V doSome(V v){
        return v;
    }
}
