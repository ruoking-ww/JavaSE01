package july.test01;

public class Emp implements Comparable {
    private String name;
    private int age;
    private String gender;
    private double salary;
    public Emp() {
    }
    public Emp(String name, int age, String gender, double salary) {
        this.name = name;
        this.age = age;
        this.gender = gender;
        this.salary = salary;
    }
    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }
    public int getAge() {
        return age;
    }
    public void setAge(int age) {
        this.age = age;
    }
    public String getGender() {
        return gender;
    }
    public void setGender(String gender) {
        this.gender = gender;
    }
    public double getSalary() {
        return salary;
    }
    public void setSalary(double salary) {
        this.salary = salary;
    }
    @Override
    public String toString() {
        return "Emp{" +
                "name='" + name + '\'' +
                ", age=" + age +
                ", gender='" + gender + '\'' +
                ", salary=" + salary +
                '}';
    }
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Emp emp = (Emp) o;

        return name != null ? name.equals(emp.name) : emp.name == null;
    }

    @Override
    public int compareTo(Object o) {

        Emp emp =null;
        try {
            emp = (Emp) o;
        } catch (Exception e) {
            System.out.println("请输入正确类型");
        }
        if(this.age > emp.age){
            return 1;
        }else if (this.age == emp.age){
            return 0;
        }
        return -1;
    }
}
