package july.test01;

import java.util.ArrayList;
import java.util.List;

public class Test002 {
    public static void main(String[] args) {

        List<String> list =  new ArrayList<>();
        list.add("ok.txt");
        list.add("hello.jpg");
        list.add("day01.rar");
        list.add("world.jpg");
        list.add("no.txt");
        System.out.println(list);
        List<String> subList = list.subList(1, 4);
        System.out.println(subList);

        for (int i = 0; i < subList.size(); i++) {
            if (subList.get(i).contains("jpg")){
                String n = subList.get(i).replace("jpg", "png");
                subList.set(i,n);
            }
        }
        System.out.println(subList);
    }
}

class List1{

    List<String> list =  new ArrayList<>();
}