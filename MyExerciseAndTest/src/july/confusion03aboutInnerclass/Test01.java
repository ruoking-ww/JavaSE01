package july.confusion03aboutInnerclass;

public class Test01 {

    public static void main(String[] args) {

        OuterClass01 o1 = new OuterClass01();
        OuterClass01.Inner01 inner01 = o1.new Inner01();
        inner01.me01();
    }
}
