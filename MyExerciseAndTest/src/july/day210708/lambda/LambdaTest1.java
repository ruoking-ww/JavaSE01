package july.day210708.lambda;

import org.junit.Test;

/**
 * Lambda表达式的使用
 *
 * 1.举例:  (o1,o2) -> Integer.compare(o1,o2);
 * 2.格式:
 *      -> 左边: lambda形参列表(其实就是接口中的抽象方法的形参列表)
 *      -> 右边: lambda体 (其实就是重写的抽象方法的方法体)
 *
 * 3.lambda表达式的使用(6种情况)
 *
 *
 * 4.lambda表达式的本质:
 *     -- 作为 接口 的 实例 --
 *
 *
 */
public class LambdaTest1 {
    //语法格式一: 无参,无返回值
    @Test
    public void test01(){
        Runnable r1 = new Runnable() {
            @Override
            public void run() {
                System.out.println("xaiobai");
            }
        };
        r1.run();

        //lambda表达式
        Runnable r2 = () -> System.out.println("xiaohei");
        r2.run();
    }
}
