package july.day210708.lambda;

import org.junit.Test;

import java.util.Comparator;

/**
 * 本类用来练习 lambda表达式
 *
 * lambda表达式的标识  ->
 *
 * 方法引用的标识  ::
 *
 */
public class Test01 {

    @Test
    public void test01(){
        Runnable r1 = new Runnable() {
            @Override
            public void run() {
                System.out.println("xaiobai");
            }
        };
        r1.run();

        //lambda表达式
        Runnable r2 = () -> System.out.println("xiaohei");
        r2.run();
    }

    @Test
    public void test02(){
        Comparator<Integer> c1 = new Comparator<Integer>() {
            @Override
            public int compare(Integer o1, Integer o2) {
                return Integer.compare(o1,o2);
            }
        };
        c1.compare(1,2);

        Comparator<Integer> c2 = (o1,o2) -> Integer.compare(o1,o2);
        int compare = c2.compare(3, 4);
        System.out.println(compare);

        //这个写法叫做 方法引用
        Comparator<Integer> c3 = Integer :: compare;
        int compare1 = c3.compare(22, 33);
        System.out.println(compare1);
    }
}
