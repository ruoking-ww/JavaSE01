package day210730.test1;

import org.junit.Test;

import java.sql.*;

public class Test1Third {

    @Test
    public void test01(){
        try {
            method();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private static void method() throws Exception {
        Class.forName("com.mysql.jdbc.Driver");

        String url = "jdbc:mysql://localhost:3306/cgb210726";
        Connection c = DriverManager.getConnection(url, "root", "123456");

        Statement statement = c.createStatement();
        ResultSet resultSet = statement.executeQuery("select * from users");

        while (resultSet.next()){
            String uid = resultSet.getString(1);
            String username = resultSet.getString(2);
            String password = resultSet.getString(3);
            System.out.println(uid+":"+username+","+password);
        }

        resultSet.close();
        statement.close();
        c.close();
    }
}
