package day210730.test1;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;

/**
 * 测试jdbc的入门案例
 *
 */
public class Test1 {
    public static void main(String[] args) throws Exception {

        //1,注册驱动
        Class.forName("com.mysql.jdbc.Driver");
        //2,获取连接
        String url = "jdbc:mysql://localhost:3306/cgb210726";
        Connection connection = DriverManager.getConnection(url, "root", "123456");
        System.out.println(connection);
        //3,获取传输器
        Statement statement = connection.createStatement();
        System.out.println(statement);
        //4,执行sql
        ResultSet set = statement.executeQuery("select * from users");
        System.out.println(set);
        //5,处理结果集
        while (set.next()){
            String s1 = set.getString(1);//获取第一列的数据
            String s2 = set.getString("username");//获取指定字段名的值

            System.out.println(s1+" "+s2);
        }
        //6,释放资源
        set.close();
        statement.close();
        connection.close();
    }
}
