package day210730.test1;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;

public class Test1Second {
    public static void main(String[] args) throws Exception {

        Class.forName("com.mysql.jdbc.Driver");

        String url = "jdbc:mysql://localhost:3306/cgb210726";
        Connection c = DriverManager.getConnection(url, "root", "123456");

        Statement statement = c.createStatement();
        ResultSet resultSet = statement.executeQuery("select * from users");

        while (resultSet.next()){
            String s1 = resultSet.getString(1);
            String s2 = resultSet.getString("username");

            System.out.println(s1+":"+s2);
        }

        resultSet.close();
        statement.close();
        c.close();
    }
}
