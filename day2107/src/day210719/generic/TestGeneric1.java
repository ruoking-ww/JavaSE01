package day210719.generic;

import java.util.ArrayList;
import java.util.List;

/**本类用于测试泛型的优点1*/
public class TestGeneric1 {
    public static void main(String[] args) {
        /**1.泛型是怎么来的？--想要模拟数组数据类型检查的功能*/
        String[] a = new String[5];//创建一个长度为5的String类型的数组
        a[0] = "泡泡";
        a[3] = "涛涛";
        /**0.数组的好处：在编译时期就检查数据的类型，只要不是规定的类型
         * 就会在编译器中报错，不需要运行程序就可以看得到*/
//        a[1] =  1;
//        a[2] =  1.6;
//        a[2] =  'c';

        /**2.泛型通常与集合一起使用*/
        List list = new ArrayList();//注意需要导包java.util...
        list.add("江江");//没有泛型的约束，数据类型太自由！！！
        list.add(1);
        list.add(1.8);
        list.add('c');

        /**3.引入泛型-主要目的是想通过泛型来约束集合中元素的类型<?> */
        /**4.泛型的好处：可以把报错的时机提前，在编译期就报错
         * 而不是运行以后才抛出异常，在向集合添加元素时，会先检查元素的类型
         * 不是要求的类型就编译失败*/
        List<String> list2 = new ArrayList<String>();
        list2.add("霞霞");//约束了泛型以后，只可以存约束的类型
//        list2.add(9);
//        list2.add(9.9);
//        list2.add(false);

        /**5.<type>--type的值应该如何去写？
         * 需要查看要存放的类型是什么，根据业务类型自定义
         * 但是type必须是引用类型，不能是基本类型*/
        //List<int> list3 = new ArrayList();//会报错
        List<Integer> list4 = new ArrayList();
        list4.add(100);
        list4.add(200);
        list4.add(300);
        System.out.println(list4);

    }

}
