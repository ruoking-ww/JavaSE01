package day210719.generic;


/**
 * 本类用于泛型的优点测试2
 *
 * 1.泛型可以写出更加通用的代码
 * 2.泛型方法 的语法要求:
 * 必须两处同时出现: 一个是方法参数类型是泛型 E[]
 * 另一个是返回值类型前的泛型<E>, 表示这是一个泛型方法
 * [注意: 类似多态, 但它不是多态]
 *
 *
 */
public class TestGeneric2 {
    public static void main(String[] args) {

        Integer[] a = {1,2,3,4,5,6,7};
        print(a);

        String[] b = {"大哥","二哥","三哥"};
        print(b);

        Double[] c = {3.0,3.1,6.6,6.666};
        print(c);
    }

    //+++++++++++++++++++++++++++++++++++++
    //泛型方法
    private static <E> void print(E[] es){
        for (E e : es) {
            System.out.println(e);
        }
    }
    //+++++++++++++++++++++++++++++++++++++

    //private static void print(Integer[] b) {
    //    //普通for循环
    //    //for (int i=0; i< b.length; i++){
    //    //    System.out.println(b[i]);
    //    //}
    //
    //    /**增强for循环
    //     * 使用场景: 只需要对数据从头到尾的遍历一次
    //     * 好处:比普通for循环更简单,更高效
    //     * 缺点: 没办法按照下标来操作元素,只能从头到尾的遍历
    //     * 语法:  for(变量类型 变量名 : 被遍历的数据){循环体}*/
    //    for (Integer integer : b) {
    //        System.out.println(integer);
    //    }
    //}
    //
    //private static void print(String[] b) {
    //    for (String s : b) {
    //        System.out.println(s);
    //    }
    //}
    //private static void print(Double[] b) {
    //    for (Double aDouble : b) {
    //        System.out.println(aDouble);
    //    }
    //}
}
