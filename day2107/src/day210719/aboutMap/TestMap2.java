package day210719.aboutMap;

import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

/**
 * 本类用于字符串中 字符个数统计案例
 *
 * 用户输入: abcccc
 * 输出结果: {a=1,b=1,c=4}
 *
 */
public class TestMap2 {

    public static void main(String[] args) {

        System.out.println("请你输入要统计的字符串");
        String input = new Scanner(System.in).nextLine();

        //创建map结构用来存储数据
        /**统计的是每个字母出现的次数,单个字符是char类型,对应的包装类是Character
         * 注意,Integer代表的是这个字母出现的次数,可以重复*/
        Map<Character,Integer> map = new HashMap<>();

        //变量用户输入的数据
        for (int i = 0; i < input.length(); i++) {
            char key = input.charAt(i);

            //统计每个字符出现的次数
            Integer value = map.get(key);
            if (value == null){
                map.put(key,1);
            }else{
                map.put(key,value+1);
            }
        }


    }
}
