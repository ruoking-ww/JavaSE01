package day210719.aboutMap;

import java.util.*;

/**
 * 本类用于测试 Map接口
 *
 *
 *
 */
public class TestMap {
    public static void main(String[] args) {

        //创建Map对象
        /**Map中的数据要符合映射规则,一定要注意同时指定K和V的数据类型
         * 至于K和V具体要指什么类型,取决于具体的业务需求*/
        Map<Integer,String> map = new HashMap<>();
        //向map集合存入数据,注意方法是 put()
        map.put(1,"小黑");
        map.put(2,"虚淮");
        map.put(3,"风息");
        map.put(4,"阿根");
        map.put(1,"玄离");  //key值相同时会把第一个覆盖掉
        /**1.map中存放的都是无序的数据
         * 2.map中的value可以重复
         * 3.map中的key不允许重复,如果重复,后面的value会把前面的value覆盖掉*/
        System.out.println(map);

        //测试方法
        //map.clear();

        System.out.println(map.hashCode());
        System.out.println(map.equals("小强"));
        System.out.println(map.isEmpty());
        System.out.println(map.size());

        System.out.println(map.containsKey(1));//true
        System.out.println(map.containsValue("小黑"));//false

        //根据key值获取到对应的value值
        System.out.println(map.get(2));

        //根据key值删除 指定的键值对
        System.out.println(map.remove(3));

        //将map集合中的所有value取出,放入Collection集合中
        Collection<String> values = map.values();
        System.out.println(values);

        /**Map集合的迭代方式
         * 方式一
         * map本身没有迭代器, 调用keySet方法, 把map中的所有key值存入到Set集合中*/
        Set<Integer> keySet = map.keySet();
        Iterator<Integer> it = keySet.iterator();
        while (it.hasNext()){
            Integer key = it.next();
            String value = map.get(key);
            System.out.println("{"+key+","+value+"}");
        }

        /**方式二
         * 把map中的一对键值对key&value作为一个Entry<K,V>整体放入Set
         * 一对K,V就是一个Entry*/
        Set<Map.Entry<Integer, String>> entries = map.entrySet();
        Iterator<Map.Entry<Integer, String>> it2 = entries.iterator();
        while (it2.hasNext()){
            Map.Entry<Integer, String> entry = it2.next();
            Integer key = entry.getKey();
            String value = entry.getValue();
            System.out.println("{"+key+","+value+"}");

        }
    }
}
