package day210719.aboutList;

import org.junit.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * 本类用于测试 List接口
 */
public class TestList {

    @Test
    public void test01(){

        //创建List的多态对象,List是接口,不可以实例化
        List<String> list = new ArrayList<>();

        //向list集合中存入数据
        list.add("老大");
        list.add("老二");
        list.add("老三");
        list.add("老四");
        list.add("老五");
        System.out.println(list);

        System.out.println(list.contains("老大")); //true
        System.out.println(list.equals("老二")); //false
        System.out.println(list.isEmpty()); //false
        System.out.println(list.remove("老五"));
        System.out.println(list.size());

        System.out.println(Arrays.toString(list.toArray()));//将集合转成数组

        //测试List接口自己的方法--List有序,可以根据索引来操作集合中的元素
        list.add("小蝴蝶");
        list.add(1,"大蝴蝶"); //在指定的索引处添加元素
        list.add(3,"小蝴蝶");

        System.out.println(list);
        System.out.println(list.indexOf("小蝴蝶"));
        System.out.println(list.lastIndexOf("小蝴蝶"));

        //根据指定索引删除元素,并将元素返回
        System.out.println(list.remove(5));
        System.out.println(list);

        System.out.println(list.get(1));//获取指定索引处的元素

        System.out.println(list.set(3,"蝎子精"));//修改指定索引处元素的值为"蝎子精"
        System.out.println(list);
    }

    @Test
    public void test02(){

        List<String> list1 = new ArrayList<>();
        list1.add("老大");
        list1.add("老二");
        list1.add("老三");
        list1.add("老四");
        list1.add("老五");

        List<String> list2 = new ArrayList<>();
        list2.add("1");
        list2.add("2");
        list2.add("3");
        list2.add("4");
        System.out.println(list2);

        System.out.println(list1.addAll(list2));

        System.out.println(list1.addAll(1,list2));
        System.out.println(list1);

        //判断list1集合中是否包含list2集合中的的所有元素
        System.out.println(list1.containsAll(list2));

        System.out.println(list1.removeAll(list2));
        System.out.println(list1);
    }
}
