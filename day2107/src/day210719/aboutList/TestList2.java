package day210719.aboutList;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;

/**
 * 本类用于进一步测试 List接口
 */
public class TestList2 {
    public static void main(String[] args) {

        List<String> list = new ArrayList<>();
        list.add("喜洋洋");
        list.add("美洋洋");
        list.add("懒洋洋");
        list.add("慢洋洋");
        list.add("灰太狼");
        System.out.println(list);

        //测试集合的迭代方式
        //1.for循环(遍历效率低,语法复杂,推荐使用高效for循环)
        System.out.println("===========方式1============");
        for (int i = 0; i < list.size(); i++) {
            System.out.println(list.get(i));
        }
        //2.增强for循环
        System.out.println("===========方式2============");
        for (String s : list) {
            System.out.println(s);
        }
        //3.iterator //从父接口中继承过来的迭代器iterator
        System.out.println("===========方式3============");
        Iterator<String> iterator = list.iterator();
        while (iterator.hasNext()){
            System.out.println(iterator.next());
        }
        //4.listIterator //它属于List接口特有的迭代器
        /*
        Iterator<E> --父接口--hasNext()  next()
        ListIterator<E> --子接口--除了父接口的功能以外,
        还有自己的特有功能,比如逆序遍历,添加元素等等,但是不常用
         */
        System.out.println("===========方式4============");
        ListIterator<String> it2 = list.listIterator();
        while (it2.hasNext()){
            System.out.println(it2.next());
        }
    }
}








