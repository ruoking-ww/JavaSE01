package day210719.aboutCollection;

import org.junit.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Iterator;

/**
 * 本类用于测试Collection接口
 *
 *
 */
public class TestCollection {

    @Test
    public void test01(){

        //创建Collection相关的对象
        Collection<Integer> col = new ArrayList<>();

        //测试集合中的常用方法--单个集合间的操作
        col.add(1); //向集合中添加元素
        col.add(2);
        col.add(3);
        col.add(4);
        col.add(4);
        System.out.println(col); //直接打印集合,查看集合中的元素

        col.clear(); //清空当前集合中的所有元素
        System.out.println(col);

        System.out.println(col.hashCode()); //获取集合对象的哈希码值
        System.out.println(col.toString()); //打印集合的具体元素
        System.out.println(col.equals(200)); //false
        System.out.println(col.contains(2)); //true //集合中是否包含指定元素
        System.out.println(col.isEmpty()); //false //判断集合是否为空
        System.out.println(col.remove(1)); //true //移除集合中的指定元素,成功返回true
        System.out.println(col.size()); //返回集合的元素个数

        Object[] array = col.toArray(); //将指定的集合转为数组
        System.out.println(Arrays.toString(array));

        Collection<Integer> c2 = new ArrayList<>();
        c2.add(2);
        c2.add(4);
        c2.add(5);
        System.out.println(c2);

        col.addAll(c2); //c2集合的所有元素添加到col集合当中
        System.out.println(col);
        System.out.println(c2);

        System.out.println("--------------------------");

        System.out.println(col.containsAll(c2));//当前集合col是否包含指定集合c2中的所有元素
        System.out.println(col.remove(5));
        System.out.println(col.containsAll(c2));

        System.out.println(col.removeAll(c2)); //删除col集合中属于c2集合的所有元素


        System.out.println(col.retainAll(c2)); //取col集合与c2集合的交集(公共元素)
        System.out.println(col);
    }

    @Test
    public void test02(){

        Collection<Integer> c = new ArrayList<>();
        c.add(2);
        c.add(4);
        c.add(5);

        //迭代集合/遍历集合
        /**迭代步骤:
         * 1.获取集合的迭代器 c.iterator();
         * 2.判断集合中是否有下一个可迭代的元素 it.hasNext();
         * 3.获取当前迭代到的元素 it.next()
         */

        Iterator<Integer> iterator = c.iterator();



        while(iterator.hasNext()){
            Integer next = iterator.next();
            System.out.println(next);
        }


    }
}



