package day210719.aboutSet;

import java.util.HashSet;
import java.util.Set;

/**
 * 本类用于测试 Set接口
 *
 * 总结: set集合中的元素都是无序的,不可重复的,可以存放null,只允许存一个
 *       自定义对象(如Student)如果想要去重,需要添加重写的hashCode()与equals() 方法
 *
 */
public class TestSet {
    public static void main(String[] args) {

        Set<String> set = new HashSet<>();

        set.add("小白");
        set.add("小蓝");
        set.add("小蓝");
        set.add(null);
        set.add("null");
        System.out.println(set);  //[null, null, 小白, 小蓝]
    }
}
