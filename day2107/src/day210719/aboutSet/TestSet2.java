package day210719.aboutSet;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

public class TestSet2 {
    public static void main(String[] args) {
        Set<String> set = new HashSet<>();
        set.add("虚淮");
        set.add("阿根");
        set.add("玄离");
        set.add("谛听");
        //set.clear();
        Set<String> set1 = new HashSet<>();
        set1.add("老君");
        set1.add("清凝");
        set1.add("玄离");
        //System.out.println(set.addAll(set1));//true
        //System.out.println(set);
        System.out.println(set.contains("老君"));
        System.out.println(set.containsAll(set1));
        System.out.println(set.isEmpty());
        System.out.println(set.remove("虚淮"));
        System.out.println(set.size());
        //System.out.println(Arrays.toString(set.toArray()));
        Iterator<String> it = set.iterator();
        while (it.hasNext()){
            System.out.println(it.next());
        }
        //for (String s : set) {
        //    System.out.println(s);
        //}
        //System.out.println(set.removeAll(set1));
        System.out.println(set.retainAll(set1));
        System.out.println(set);
    }
}
