package day210719.aboutList1;

import java.util.ArrayList;
import java.util.List;

/**
 * 本类用于测试 ArrayList
 *
 *
 *
 */
public class TestArrayList {

    public static void main(String[] args) {

        /**底层会自动帮我们创建数组来存放对象,并且数组的初始容量是10*/
        List<Integer> list = new ArrayList<>();
        list.add(1);
        list.add(2);
        list.add(3);
        list.add(4);
        list.add(4);
        list.add(3);
        System.out.println(list);
        //list.clear();//清空集合

        System.out.println(list.contains("1")); //false
        System.out.println(list.isEmpty());
        System.out.println(list.get(0));
        System.out.println(list.indexOf(1));
        System.out.println(list.lastIndexOf(3));

        //public E remove(int index)
        //这里的1不是移除集合里面的1,而是移除集合里面索引为1的元素
        System.out.println(list.remove(1));

        //public boolean remove(@Nullable Object o)
        //这样写的话移除的就是集合里面元素为1的数据
        //int类型的1被手动装箱成Integer类型
        System.out.println(list.remove(Integer.valueOf(1)));

        System.out.println(list.set(2, 22));
        System.out.println(list);

        //进行集合的迭代
        System.out.println("方式一: for循环迭代");
        for (int i = 0; i < list.size(); i++) {
            System.out.println(list.get(i));
        }

        System.out.println("方式二: 高效for循环迭代");

    }
}
