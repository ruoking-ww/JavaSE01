package day210719.aboutList1;

import java.util.LinkedList;

/**
 * 本类用于测试 LinkedList
 *
 *
 */
public class TestLinkedList {
    public static void main(String[] args) {

        //1.创建集合对象
        LinkedList<String> list = new LinkedList<>();

        list.add("小白");
        list.add("小黑");
        list.add("山新");
        list.add("阿根");
        list.add("无限");
        System.out.println(list);

        //测试LinkedList自己的方法
        list.addFirst("谛听");
        System.out.println(list);
        list.addLast("玄离");
        System.out.println(list);

        System.out.println(list.getFirst());//获取首元素
        System.out.println(list.getLast());//获取尾元素

        System.out.println(list.removeFirst());//移除首元素
        System.out.println(list.removeLast());//移除尾元素
        System.out.println(list);



        //4.其他测试
        //4.1创建集合对象并存入数据
        LinkedList<String> list2 = new LinkedList<>();
        list2.add("水浒传");
        list2.add("西游记");
        list2.add("三国演义");
        list2.add("红楼梦");
        System.out.println(list2);
        //获取但不移除集合中的首元素(第一个元素)
        System.out.println(list2.element());//水浒传
        /**别名：查询系列*/
        //获取但不移除集合中的首元素(第一个元素)
        System.out.println(list2.peek());
        System.out.println(list2.peekFirst());
        //获取但不移除集合中的尾元素(最后一个元素)
        System.out.println(list2.peekLast());

        /**别名：新增系列*/
        System.out.println(list2.offer("聊斋志异"));
        System.out.println(list2.offerFirst("斗罗大陆"));
        System.out.println(list2.offerLast("斗破苍穹"));
        System.out.println(list2);

        /**别名：移除系列*/
        System.out.println(list2.poll());//移除首元素
        System.out.println(list2.pollFirst());
        System.out.println(list2.pollLast());
        System.out.println(list2);
    }

}
