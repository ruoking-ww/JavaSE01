package day210722.aboutDesignMode;

/**
 * 本类用于测试单例设计模式方案2--懒汉式--面试重点
 *
 *
 */
public class TestSingleton2 {

    public static void main(String[] args) {

        MySingle2 s1 = MySingle2.getSingle2();
        MySingle2 s2 = MySingle2.getSingle2();
        System.out.println(s1);
        System.out.println(s2);
    }
}

//创建单例程序
class MySingle2{

    private MySingle2(){}

    static MySingle2 single2;

    public static MySingle2 getSingle2(){
        if (single2 == null){
            //如果为null,说明之前没有创建过对象
            single2 = new MySingle2();
        }
        return single2;
    }
}