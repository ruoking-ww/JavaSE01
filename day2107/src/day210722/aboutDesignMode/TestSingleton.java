package day210722.aboutDesignMode;

/**
 * 本类用于测试单例设计模式方案1
 *
 * 饿汉式
 */
public class TestSingleton {
    public static void main(String[] args) {

        MySingle single = MySingle.getSingle();
        MySingle single1 = single.getSingle();
        System.out.println(single == single1); //true
    }
}

//创建自己的单例程序
class MySingle{

    //提供构造方法,并将构造方法私有化
    /**构造方法私有化,不让外界调用创建对象*/
    private MySingle(){}

    //创建本类对象,并将其私有化
    /**对象私有化: 为了不让外界直接获取创建好的对象*/
    private static MySingle single = new MySingle();

    //提供公共的全局访问点,供外界调用获取对象
    public static MySingle getSingle(){
        return single;
    }
}