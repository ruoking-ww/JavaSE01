package day210722.aboutReflection;

import org.junit.Test;

/**
 * 本类用作反射技术测试类
 *
 *
 */
public class TestReflection {

    //单元测试方法: 是java运行的最小单位,使用灵活
    //注意: 使用时需要导包: Add JUnit4 to classpath
    @Test
    public void getClazz() throws ClassNotFoundException {
        Class<?> student1 = Class.forName(
                "day210722.aboutReflection.AboutStudent");

        Class<?> student2 = AboutStudent.class;

        Class<?> student3 = new AboutStudent().getClass();

        System.out.println(student1);//class day210722.aboutReflection.AboutStudent
        System.out.println(student2.getName());//day210722.aboutReflection.AboutStudent
        System.out.println(student3.getSimpleName());//AboutStudent
        System.out.println(student1.getPackage());//package day210722.aboutReflection
        System.out.println(student1.getPackage().getName());//day210722.aboutReflection
    }
}
