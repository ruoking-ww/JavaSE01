package day210722.aboutReflection;

/**
 * 用作反射测试的物料类--假装这是别人的代码,不是自己写的
 *
 *
 */
public class AboutStudent {

    String name;
    int age;

    public AboutStudent() {
    }
    public AboutStudent(String name, int age) {
        this.name = name;
        this.age = age;
    }

    public void eat(int n){
        System.out.println("吃了"+n+"碗(๑′ᴗ‵๑)Ｉ Lᵒᵛᵉᵧₒᵤ大米饭");
    }
}
