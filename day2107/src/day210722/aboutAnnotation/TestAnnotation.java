package day210722.aboutAnnotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * 本类用于实现 自定义注解
 *
 *
 * 注解定义格式: @interface 注解名 { }
 *   (注解名是自定义的,可以随意设定)
 * 注意:自定义注解的语法与java不同,不要套用java的格式
 *
 *
 * 元注解: 用来定义注解的注解
 *  @Target 注解用在哪里：类上、方法上、属性上等等
 *     (自定义注解能够加在什么位置,取决于@Target注解的值)
 *  @Retention 注解的生命周期：源文件中 SOURCE、字节码文件中 CLASS、运行中 RUNTIME
 *                  (注意:这三个值只能三选一)
 *
 *     如果注解(里面)有多个值,使用{ } 包起来
 *
 *
 */
public class TestAnnotation {

}

@Test1(age=1,value="da")
class TestAnno{

    //@Test1
    String name;

    @Test1(age=3,value="dada")
    public void eat(){
        System.out.println("....");
    }
}

@Retention(RetentionPolicy.SOURCE)
@Target({ElementType.METHOD,ElementType.TYPE})
@interface Test1{
    /*
    注意 int age(); 是注解中定义的属性
    再次注意: 注解没有定义属性时可以直接使用,如果添加了属性,就必须给属性赋值
    给注解的属性赋值的格式 比如 @Test1(age=10)

    注解定义属性时,还可以给属性设置默认值,这样使用注解时,可以不用赋值,
    使用默认值

    我们还可以给自定义注解添加特殊属性 value
    特殊属性的定义格式与普通属性一样,主要区别在于使用时
    使用此注解给属性赋值时,可以直接写值 比如 @Test1("apple")

    注解里面 有多个属性时, 赋值就要写完整了 比如 @Test1(age=10,value="apple")
     */

    //int age();
    int age() ;

    String value() ;
}