package day210713.aboutInterface;

/**
 * 本类用作Interface01接口的 实现类
 *
 * 1.接口实现类通过implements关键字与接口建立实现关系
 *    1.1接口与实现类建立实现关系以后,可以选择不实现接口中的抽象方法,
 *       而是通过添加abstract关键字把自己变成一个抽象类
 *    1.2实现接口中的所有抽象方法,变成一个普通实现类
 *
 *
 */
//abstract public class Interface01Impl implements Interface01{ }
public class Interface01Impl implements Interface01{

    @Override
    public void eat() {
        System.out.println("吃水果");
    }
}