package day210713.aboutInterface;

/**
 * 本类用于进一步测试 接口的使用
 *
 * 1.接口中没有构造方法
 *
 * 2.接口中没有成员变量!!
 *     接口中的(成员)变量实际上是一个 ##静态常量##
 *     可以通过接口名直接调用,不能被重新赋值
 *     [实际上省略了 static final 关键字]
 * //inter2.age = 100;  //报错:Cannot assign a value to final variable 'age'
 *
 * 3.接口中都是抽象方法,而且 public与abstract可以简写(就是省略不写,但实际上有)
 *
 * 当一个类没有明确指定父类的情况下,默认的父类是顶级父类Object
 *
 * 接口实现类的 父级接口并不是一个类,接口没有构造函数,实现类构造方法中的super()
 * 调用的是继承的父类的无参构造,如果没有明确指定父类,那么父类便默认是Object
 *
 * [接口实际上就是一个特殊类]
 */
public class TestUserInter {
    public static void main(String[] args) {

        Inter2Impl inter2 = new Inter2Impl();
        int age1 = inter2.age;
        System.out.println(age1);

        Inter2 inter = new Inter2Impl();
        int age = inter.age;
        System.out.println(age+"nihao");

        System.out.println(Inter2.age);
        //System.out.println(Inter2Impl.age);

        //inter2.age = 100;  //报错:Cannot assign a value to final variable 'age'

        Pad pad = new Pad();
        int a = pad.a;
    }
}

//创建接口
interface Inter2{
    int age = 10;

    void eat();

    void play();
}
class Inter2Impl implements Inter2{
    int age = 100;
    public Inter2Impl(){
        super();
        System.out.println("我是实现类的构造方法");
    }
    @Override
    public void eat() {
    }
    @Override
    public void play() {
    }
}
class Pad{
    int a = 10;
}