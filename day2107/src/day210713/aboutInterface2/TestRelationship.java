package day210713.aboutInterface2;

/**
 * 本类用于测试接口与类之间的复杂关系
 *
 * 1. 接口之间可以建立继承关系,而且可以多继承,多个接口使用逗号隔开
 *
 * 2. 接口与实现类之间是实现关系
 *    注意,对于Java的类而言,遵循"单继承,多实现"的原则
 *    即 一个类只能有一个父类,但一个类可以实现多个接口
 *
 *
 *
 */
public class TestRelationship {
}

//创建接口1
interface Inter1{
    void save();    //新增功能
    void delete();  //删除功能
}
//创建接口2
interface Inter2{
    void find();    //查找功能
    void update();  //更新功能
}
//创建接口3
interface Inter3 extends Inter1,Inter2{

}

//创建接口实现类
class Inter3Impl implements Inter3{

    @Override
    public void save() {
        System.out.println("保存ing..");
    }

    @Override
    public void delete() {
        System.out.println("删除");
    }

    @Override
    public void find() {
        System.out.println("查找");
    }

    @Override
    public void update() {
        System.out.println("更新");
    }
}