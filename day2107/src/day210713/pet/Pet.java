package day210713.pet;

/**
 * 设计一款电子宠物,用户可以自由选择养猫还是养狗..
 */

//创建父类 宠物类
public class Pet {
    String name;  //名字
    int full;     //饱食度
    int happy;    //开心度

    public Pet(String name) {
        //含参中调用全参给饱食度和开心度设置初始值为50
        this(name,50,50);
    }
    public Pet(String name, int full, int happy) {
        this.name = name;
        this.full = full;
        this.happy = happy;
    }

    //创建电子宠物的喂养功能
    public void feed(){

        if(full == 100){
            System.out.println(name+"已经吃饱了,不要再喂啦");
            return;  //return关键字结束方法
        }

        System.out.println("给"+name+"喂食");
        full += 10;
        System.out.println("饱食度:"+full);
    }
    //创建电子宠物的玩功能
    public void play(){
        if (full == 0){
            System.out.println(name+"已经额的玩不动了");
            return;
        }
        System.out.println("陪"+name+"开心的玩耍");
        happy += 10; //开心度+10
        full -= 10;
        System.out.println("快乐度:"+happy);
        System.out.println("饱食度:"+full);
    }
    //创建电子宠物的惩罚功能
    public void punish(){
        System.out.println("打"+name+"的pp,哭声:"+cry());
        happy -= 10;
        System.out.println("快乐度:"+happy);
    }
    //创建电子宠物哭功能
    public String cry(){

        //cry方法需要在子类中重写
        return "此处有哭叫声";
    }
}
