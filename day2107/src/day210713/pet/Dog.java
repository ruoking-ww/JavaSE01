package day210713.pet;

public class Dog extends Pet {

    //创建构造函数
    //子类构造函数首行默认是super(),创建子类对象时必须要调用父类的构造函数
    //而Pet类中没有无参构造,所以需要手动添加调用父类含参构造的子类构造函数
    public Dog(String name,int full,int happy){
        super(name,full,happy);
    }
    public Dog(String name){
        super(name);
    }
    @Override
    public String cry(){  //重写父类中方法
        return "汪汪汪";
    }
}
