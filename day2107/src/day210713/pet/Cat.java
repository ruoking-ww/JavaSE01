package day210713.pet;

public class Cat extends Pet {

    //创建构造函数
    //子类构造函数首行默认是super(),创建子类对象时必须要调用父类的构造函数
    //而Pet类中没有无参构造,所以需要手动添加调用父类含参构造的子类构造函数
    public Cat(String name,int full,int happy){
        super(name,full,happy);
    }
    public Cat(String name){
        super(name);
    }
    @Override
    public String cry(){
        return "喵喵喵";
    }
}
