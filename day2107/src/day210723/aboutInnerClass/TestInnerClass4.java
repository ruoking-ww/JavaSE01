package day210723.aboutInnerClass;

/**
 * 本类用于测试 局部内部类
 *
 * 如何使用局部内部类的资源呢?
 * 在外部类中创建局部内部类的对象并且调用它的功能
 *
 *
 */
public class TestInnerClass4 {
    public static void main(String[] args) {

        Outer4 outer4 = new Outer4();
        outer4.show();
    }
}


class Outer4{

    public void show(){
        System.out.println("....");

        class Inner4{
            String name;
            int age;
            public void eat(){
                System.out.println("我是局部内部类的一个普通方法");
            }
        }

        //如何使用局部内部类的资源呢?
        //在外部类中创建局部内部类的对象并且调用它的功能
        Inner4 inner4 = new Inner4();
        inner4.eat();
        System.out.println(inner4.name);
    }
}