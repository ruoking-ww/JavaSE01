package day210723.aboutInnerClass;

/**
 * 内部类可以直接使用外部类 的资源
 */
public class TestInnerClass {
    public static void main(String[] args) {

        Outer.Inner inner = new Outer().new Inner();
        inner.delete();
        System.out.println(inner.sum);


    }
}

class Outer{
    String name;
    private int age;

    public void find(){
        System.out.println("我是一个成员方法");

        //System.out.println(sum);

        //外部类要使用内部类资源 先创建内部类对象,然后调用
        Inner inner = new Inner();
        inner.delete();
    }

    class Inner{
        int sum = 100;
        public void delete(){
            System.out.println("我是内部类的一个方法");

            System.out.println(name);
            System.out.println(age);

            /**注意:方法之间不能来回调用,会报错: 栈溢出错误*/
            //find();
        }
    }
}