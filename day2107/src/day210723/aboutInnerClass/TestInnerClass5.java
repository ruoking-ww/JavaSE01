package day210723.aboutInnerClass;

/**
 * 本类用于测试 匿名内部类
 *
 *
 */
public class TestInnerClass5 {

    public static void main(String[] args) {


        //大括号里面 就是匿名内部类
        //匿名对象只能调用一次方法
        new Inner11() {
            @Override
            public void save() {
                System.out.println("你好");
            }

            @Override
            public void get() {

            }
        }.save();

        new A() {
            @Override
            public void come() {
                System.out.println("nonono");
            }
        }.come();
    }
}

interface Inner11{

    void save();
    void get();
}

abstract class A{

    public abstract void come();
    public void go(){
        System.out.println("ff");
    }
}