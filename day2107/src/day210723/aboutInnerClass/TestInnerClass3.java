package day210723.aboutInnerClass;

/**
 * 本类用于测试成员内部类被static修饰
 *
 *
 */
public class TestInnerClass3 {
    public static void main(String[] args) {

        //Outer3.Inner3 inner3 = new Outer3().new Inner3();
        //inner3.show();

        //创建内部类匿名对象并调用功能
        //new Outer3().new Inner3().show();

        /**当内部类被static修饰以后,new Outer3()报错*/
        //创建静态内部类的普通对象 //把Outer 的小括号去掉,
        // 内部类作为外部类的一个静态资源,直接被外部类类名调用
        Outer3.Inner3 inner3 = new Outer3.Inner3();
        inner3.show();

        //创建静态内部类的匿名对象
        new Outer3.Inner3().show();


        //访问静态内部类的静态资源--链式加载
        Outer3.Inner3.show2();
    }
}



class Outer3{


    static class Inner3{

        public void show(){
            System.out.println("我是一个内部类方法");
        }


        static public void show2(){
            System.out.println("我是一个内部类方法2");
        }
    }
}