package day210723.aboutInnerClass;

/**
 * 本类用于测试 成员内部类被private修饰
 *
 */
public class TestInnerClass2 {
    public static void main(String[] args) {

        //内部类被private修饰后,其他类里面就不可以直接new了
        //Outer2.Inner2 inner2 = new Outer2().new Inner2();
        //inner2.eat();
    }
}


class Outer2{


    //成员内部类//类里方法外
    private class Inner2{
        public void eat(){
            System.out.println("我是成员内部类的普通方法");
        }
    }

    //内部类被私有后,提供一个公共的访问方法,调用其方法
    public void getInner2Eat(){
        Inner2 inner2 = new Inner2();
        inner2.eat();
    }
}
