package day210723.reflection02;

import org.junit.Test;

import java.lang.reflect.Field;
import java.lang.reflect.Method;

/**
 * 本类用于测试暴力反射
 *
 */
public class TestReflection03 {

    //通过暴力反射获取私有属性
    @Test
    public void test01() throws Exception {

        Class<?> clazz = Person.class;

        Field field = clazz.getDeclaredField("name");

        //根据获取到的属性对象 获取对应的信息
        System.out.println(field.getName()); //name
        System.out.println(field.getType()); //class java.lang.String
        System.out.println(field.getType().getName()); //java.lang.String

        /**设置私有资源可见的权限*/
        field.setAccessible(true);

        //想给指定的属性设置值
        //set(m,n)-- m 要给哪个对象设置, n 要设置的具体的值
        Object o = clazz.newInstance();
        field.set(o,"zhangsan");

        //System.out.println(field.get(o));
        //java.lang.IllegalAccessException: Class day210723.reflection02.TestReflection03 can not access a member of class day210723.reflection02.Person with modifiers "private"

        System.out.println(field.get(o));
    }

    //通过暴力反射 获取和使用私有方法
    @Test
    public void test02() throws Exception {

        Class<?> clazz = Person.class;

        Method find = clazz.getDeclaredMethod("find", String.class);

        //需要确定哪个对象来执行获取到的find(),先获取一个对象
        Object o = clazz.newInstance();

        find.setAccessible(true);

        find.invoke(o,"小猫");
    }
}
