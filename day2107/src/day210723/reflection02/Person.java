package day210723.reflection02;

/**
 * 本类用作测试暴力反射的物料类
 *
 *
 */
public class Person {

    private String name;
    private int age;

    private void find(String s){
        System.out.println("发现了"+s);
    }
    private void update(){
        System.out.println("刷新中...");
    }
}
