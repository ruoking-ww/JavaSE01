package day210723.reflection01;

public class Student03 {

    public String name;
    public int age;
    public Student03() {
    }
    public Student03(String name, int age) {
        this.name = name;
        this.age = age;
    }
    public void eat(int n){
        System.out.println("今天吃了"+n+"碗饭!");
    }
    public void play(){
        System.out.println("niaho");
    }
    @Override
    public String toString() {
        return "Student03{" +
                "name='" + name + '\'' +
                ", age=" + age +
                '}';
    }
}
