package day210723.reflection01;

import org.junit.Test;

import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.Arrays;

public class TestReflection02 {

    //通过反射获取 目标类对象
    @Test
    public void test05() throws Exception {

        //1.获取目标类的字节码对象
        Class<?> clazz = Student03.class;

        //创建对象
        /**------------------------------------------------
         * newInstance()方法触发的是无参构造来创建目标类对象
         * ------------------------------------------------
         */
        Object o = clazz.newInstance();
        System.out.println(o); //day210723.reflection01.Student03@50134894

        Object stu = new Student03();
        System.out.println(stu); //day210723.reflection01.Student03@2957fcb0


        /**
         * --------------------------------------------------------
         * 创建对象,想触发对应的含参构造来创建对象,
         * 需要使用Class类的getConstructor方法得到Constructor对象
         * 使用Constructor类的newInstance方法可以得到Student03的含参对象
         * Constructor<T> getConstructor(@Nullable Class<?>... parameterTypes)
         * --------------------------------------------------------
         */
        Constructor<?> constructor = clazz.getConstructor(String.class, int.class);
        Object obj = constructor.newInstance("小明", 3);
        System.out.println(obj);

        /**----------------------------------------------
         * 想要查看对象的具体属性值,需要把obj强制类型转换为Student03
         * 因为多态中,父类不可以直接使用子类的特有属性与功能
         * 需要把Object强转为Student03  //因为Student03继承Object,所以可以强转
         * -----------------------------------------------
         */
        Student03 s = (Student03) obj;
        System.out.println(s.name);
    }

    //获取字节码对象与对应资源的名字
    @Test
    public void test01() throws ClassNotFoundException {

        Class<?> student1 = Class.forName("day210723.reflection01.Student03");
        Class<?> student2 = Student03.class;
        Class<?> student3 = new Student03().getClass();

        System.out.println(student1);
        System.out.println(student1.getName());
        System.out.println(student1.getSimpleName());
        System.out.println(student1.getPackage());
        System.out.println(student1.getPackage().getName());
    }

    //获取目标类Student03的构造方法
    @Test
    public void test02(){

        Class<?> clazz = Student03.class;

        //获取全部的构造方法对象,是一个数组
        Constructor<?>[] cs = clazz.getConstructors();

        //遍历数组,获取每个构造方法
        for (Constructor<?> c : cs) {
            System.out.println(c.getName());
            Class<?>[] types = c.getParameterTypes();
            System.out.println(Arrays.toString(types));
            System.out.println("--------------");
        }
    }

    //获取目标类的普通方法
    @Test
    public void test03() throws ClassNotFoundException {

        Class<?> clazz = Class.forName("day210723.reflection01.Student03");

        //获取目标类中的普通方法,并存入ms数组
        Method[] ms = clazz.getMethods();

        for (Method m : ms) {
            System.out.println(m.getName());

            Class<?>[] types = m.getParameterTypes();
            System.out.println(Arrays.toString(types));
            System.out.println("----------");
        }
    }

    //获取目标类的成员变量
    @Test
    public void test04(){

        Class<?> clazz = Student03.class;

        Field[] fields = clazz.getFields();

        //注意: 此时成员变量的修饰符必须是public才能被获取到
        for (Field field : fields) {
            System.out.println(field.getName());//获取遍历到的属性名
            System.out.println(field.getType().getName());//获取遍历到的属性类型名
            System.out.println("------");
        }
    }
}
