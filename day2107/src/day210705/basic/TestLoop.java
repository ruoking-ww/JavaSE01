package day210705.basic;

import org.junit.Test;

/**
 * 三种循环
 */
public class TestLoop {

/**
 * 测试for循环
 */
	@Test
	public void forLoop(){
//		格式: for(;;){}
		for(int i=0; i<10; i++){
//			循环体
			System.out.println(i);
		}
	}
	
/**
 * 局部变量跟随方法,方法结束,这个变量也就被清除了
 */
//	双重循环(嵌套循环)
	@Test
	public void forLoop2(){
/**
 * 代码块中定义的变量,当代码块结束,他就被java干掉了, for循环体也是一个代码块
 */
		/*for(int i=1; i<10; i++){
			System.out.print(i);
		}
		System.out.println();
		for(int i=1; i<10; i++ ){
			System.out.print(i);
		}*/
		
/**
 * 外循环i 控制有多少行
 * 内循环j 控制每行有多少个
 */
//		第一层循环,外循环
		for(int i=1; i<10; i++){
//			第二层循环,内循环
			for(int j=1; j<i+1; j++){
				System.out.print(i+"*"+j+"="+i*j+" ");
			}
			
			System.out.println();
		}
	}


/**
 * 测试while循环
 */
	@Test
	public void whileLoop(){
		int i = 1;
		while(i<10){
			System.out.println(i);
			i++;
		}
	}
	
	
/**
 * 测试do-while
 * 
 * while循环是先判断, do-while循环是后判断
 */
	@Test
	public void doLoop(){
		int i = 1;
		do{
			System.out.println(i);
			i++;
		}while(i<10);
	}
}






















