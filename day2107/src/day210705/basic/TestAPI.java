package day210705.basic;

import org.junit.Test;

/**
 * 本类用于测试 String类的相关方法
 *
 * 案例: 将类名首字母小写 并输出
 *
 *
 *
 */
public class TestAPI {

	//转义字符:  \n  \t  \b
	@Test
	public void convert(){
		System.out.println("n");
		System.out.println("==\n=="); //  \n 换行
		System.out.println("==\t=="); //  \t table键
		System.out.println("==\b=="); //  \b 退格键
	}
	
/**
 * concat 字符串连接符
 */
	@Test
	public void concat(){
		String s1 = "王";
		String s2 = "隔壁";
		
		System.out.println(s1+s2);
		System.out.println(s1.concat(s2));
		
		//打印字符串长度
		System.out.println(s1.length());
		System.out.println(s2.length());
		System.out.println((s1+s2).length());
		System.out.println(s1.concat(s2).length());
	}

/**
 * trim 截取字符串两边的空格,支持多个
 */
	@Test
	public void trim(){
		String s = "   a  b  v  ";
		System.out.println(s.trim());
	}

/**
 * charAt 获取字符串中的某个字符
 */
	@Test
	public void charAt(){
		String name = "陈强";
		System.out.println(name.charAt(0));
		
//		获取每一个字符, for循环遍历
		String s = "富强,民主,敬业,文明,友好,法治";
		int len = s.length();
		for(int i=0; i<len; i++){
			System.out.print(s.charAt(i));
		}
	}
	
	@Test
	public void substring(){
		String name = "小偶木";
		String a = name.substring(1,3); //偶木
		System.out.println(a);
	}
	
/**
 * 将类名首字母小写 并输出	
 */
	@Test
	public void toCase(){
		
//		获取当前全类名
		String className = this.getClass().getName();
		System.out.println(className);//cn.tedu.api.TestAPI
		
//		截取类名字符串
		int pos = className.lastIndexOf(".");
		System.out.println(pos);
		
		className = className.substring(pos+1);
		System.out.println(className);
		
//		获取类名首字母 并转小写
		String s1 = ""+className.charAt(0);
		s1 = s1.toLowerCase();
		String s2 = className.substring(1);
		
//		将小写首字母和后面的内容拼接
		System.out.println(s1.concat(s2));
		
	}
	
}





























