package day210705.basic;

import org.junit.Test;

/**
 * 异常:
 * 1) Error 错误,系统级别. 开发者无法处理
 * 2) Exception 异常,业务逻辑错误
 * 
 * 如果没有专门去处理异常,执行某行出错后,后面的代码不执行
 * 
 */
public class TestException {

//	分母为0,程序出错
//	自己处理异常,
	
	@Test
	public void zero(){
		
//		包裹可能出错的代码
		try{
			int i= 10/0;
			System.out.println(i);
		}catch(Exception e){
//			捕获异常,并没有真正解决异常,只是把错误信息输出到控制台(日志)
			System.out.println(e.getMessage());
		}
	}
	

	
	@Test
	public void array(){
		try{
			int[] arr = new int[2];
			System.out.println(arr[5]);
		}catch(Exception e){
//			System.out.println(e.getMessage());
			e.printStackTrace();
		}finally{
			System.out.println("执行finally");
		}               
	}
	
	@Test
	public void test01(){
		int[] arr = new int[2];
		System.out.println(arr[2]);
	}
}
















