package day210705.basic;

import org.junit.Test;

/**
 * if分支判断语句
 */
public class TestIf {

	@Test
	public void testIf() {
		// 奇偶数判断
		int n = 10;
		if (n % 2 == 1) { // 括号内结果是布尔值
			System.out.println("奇数");
		} else {
			System.out.println("偶数");
		}

		// if后面没有括号,只执行紧跟着的一句,有括号则执行括号内的
		boolean isRainning = true;
		if (isRainning) {
			System.out.println("打伞");
		}
		if (isRainning) {
			System.out.println("打伞");
			System.out.println("穿雨鞋");
		}
	}

	@Test
	public void elseIf() {
		int phone = 110;
		if (phone == 110) {
			System.out.println("警察");
		} else if (phone == 114) {
			System.out.println("查号");
		} else if (phone == 119) {
			System.out.println("消防");
		} else if (phone == 120) {
			System.out.println("急救");
		} else {
			System.out.println("电话号码错误");
		}
	}
}
































