package day210705.basic;

import org.junit.Test;

/**
 * 退出循环,3种方式: 
 * break: 跳出并结束循环,
 * continue: 跳出本次循环,
 * return: 结束方法
 */
public class TestExitLoop {

	@Test
	public void exit(){
		int i = 0;
		while(i<6){
			i++;
			if(i == 3){
//				break;
//				continue;
				return;
			}
			System.out.println(i);
		}
		
		System.out.println("执行完成");
	}
}
