package day210705.basic;

import java.util.Scanner;

import org.junit.Test;

/**
 * 业务需求:
 * 输入1: 查询
 * 输入2: 取款
 * 输入3: 存款
 * 输入4: 退出(程序)
 */
public class ATM_loop {

	@Test
	public void atm(){
//		人工输入: 等待键盘输入api, Scanner, 
//		阻塞,程序不会继续执行,需要输入回车
//		创建Scanner对象
//		键盘输入: System.in
		Scanner scan = new Scanner(System.in);
//		怎么获取键盘输入 的内容,通过.操作符获取对应的 值
//		scan.nextLine()  
		
		while(true){
		
			System.out.println("请输入你的指令:");
			int num = scan.nextInt(); //把键盘输入的字符串转换成整型
			
	//		int num = 1;
			switch(num){
			case 1:
				System.out.println("查询");
				break;
			case 2:
				System.out.println("取款");
				break;
			case 3:
				System.out.println("存款");
				break;
			case 4:
				System.out.println("退出");
				return;
			}
		}
	}
}





























