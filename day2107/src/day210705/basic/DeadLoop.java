package day210705.basic;

import org.junit.Test;

/**
 * java提供的两种经典死循环的写法
 *
 *
 */
public class DeadLoop {

	@Test
	public void forDeafLoop(){
		for(;;){
			System.out.println("for死循环");
		}
	}
	
	@Test
	public void whileDeadLoop(){
		while(true){
			System.out.println("wihle死循环");
		}
	}
}
