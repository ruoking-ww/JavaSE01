package day210705.basic;

import org.junit.Test;

/**
 * 多分支 switch,语法结构和 else-if 实现同样功能
 * 
 * 注意: switch不支持 long 型
 */
public class TestSwitch {

//	switch一旦有一个条件成立,后面的条件都不再判断
	@Test
	public void phone(){
//		判断电话号码
		int phone = 119;
		switch(phone){	//括号内是 一个 (变量)值
		case 110:
			System.out.println("警察");
			break;	//跳出switch
		case 114:
			System.out.println("查号");
			break;
		case 119:
			System.out.println("火警");
			break;
		case 120:
			System.out.println("医院");
			break;
		default :
			System.out.println("电话号码错误");
		}
	}
}
