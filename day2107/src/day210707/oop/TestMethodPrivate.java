package day210707.oop;

/**
 * 关于 private
 *
 *
 */
public class TestMethodPrivate {
    public static void main(String[] args) {
        Dog dog = new Dog();
        dog.play();
    }
}

class Dog{
    private void eat(){
        System.out.println("吃肉");
    }
    public void play(){
        System.out.println("玩飞碟");
        eat();
    }
}
