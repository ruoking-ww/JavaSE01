package day210707.oop;

/**本类用于面向对象第一天综合练习*/
public class TestOOP {
    public static void main(String[] args) {
        //在main方法中创建对象进行测试
        Person p = new Person();
        p.setName("安吉拉大宝贝");
        p.setAge(18);
        p.setGender("白富美");
        p.setHeight(168.88);
        p.setAddress("江南");
        System.out.println(p.getName());
        System.out.println(p.getAge());
        System.out.println(p.getGender());
        System.out.println(p.getHeight());
        System.out.println(p.getAddress());

        p.study();
    }
}

class Person{
    private String name;
    private String gender;
    private double height;
    private String address;
    private int age;

    private void eat(){
        System.out.println("学习过后就干饭");
    }
    public void study(){
        System.out.println("保持学习的习惯~");
        eat();
    }

    //给封装的属性提供对应的get set方法
    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }
    public String getGender() {
        return gender;
    }
    public void setGender(String gender) {
        this.gender = gender;
    }
    public double getHeight() {
        return height;
    }
    public void setHeight(double height) {
        this.height = height;
    }
    public String getAddress() {
        return address;
    }
    public void setAddress(String address) {
        this.address = address;
    }
    public int getAge() {
        return age;
    }
    public void setAge(int age) {
        this.age = age;
    }
}














