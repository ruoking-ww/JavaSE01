package day210707.oop;

/**
 * 本类用于练习
 */
public class TestCreateClass2 {
    public static void main(String[] args) {
        Car car = new Car();
        //调用汽车类对象的方法
        car.start();
        //修改汽车类对象的属性
        car.stop();
        //查看修改后汽车类的属性
        car.brand = "bmw";
        car.color = "black";
        car.price = 5.5;
        System.out.println(car.brand);
        System.out.println(car.color);
        System.out.println(car.price);
    }
}

//1.创建一个默认修饰符的类
class Car{
    //2.定义特征--成员变量--类里方法外
    String brand;
    String color;
    double price;

    //3.定义功能--方法
    public void start(){
        System.out.println("我的车车启动了");
    }
    public void stop(){
        System.out.println("熄火了");
    }
}
