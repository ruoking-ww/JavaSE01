package day210707.oop;

/**
 * 本类用于测试面向对象 之 封装
 *
 *
 */
public class TestEncapsulation {
    public static void main(String[] args) {
        Student stu = new Student();
        //给对象的属性赋值
        stu.setSubject("大数据");
        System.out.println(stu.getSubject());
        stu.setId(101);
        System.out.println(stu.getId());
        stu.setName("小明");
        System.out.println(stu.getName());

        //通过对象调用方法
        stu.study();
        //stu.play();
    }
}

//1.通过class关键字描述学生这一类对象
class Student{
    //2.成员变量
    private int id;
    private String name;

    /**被private修饰的资源是私有资源
     * 私有资源只能在本类中访问,外部无法访问*/
    //练习属性的封装
    private String subject; //科目

    /**关于方法的第四个要素:返回值类型
     * 如果一个方法需要返回结果,那么通过return关键字返回
     * 注意返回值类型String必须与返回值subject的类型保持一致*/
    public String getSubject() {
        return subject;
    }
    public void setSubject(String subject) {
        this.subject = subject;
    }
    public int getId() {
        return id;
    }
    public void setId(int id) {
        this.id = id;
    }
    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }

    public void study() {
        System.out.println("学习");
        play();
    }
    private void play() {
        System.out.println("玩啦");
    }
    //3.方法
}