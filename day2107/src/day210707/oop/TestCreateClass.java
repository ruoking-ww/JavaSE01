package day210707.oop;

/**
 * 本类用于练习面向对象入门案例
 *
 * 需求:设计手机这一类事物
 * 特性:品牌,价格,尺寸,颜色
 * 功能:打电话,发短信,看直播
 */

//* 在一个java文件当中可以创建多个class
//* 但是被public修饰的class只能有一个
//* 而且这个公共类的名字就是文件的名字

public class TestCreateClass {
    //4.创建程序的入口函数
    public static void main(String[] args) {
        //我们通过关键字new 来创建对象
        //5.创建手机类的对象
        Phone p = new Phone();
        Phone p2 = new Phone();
        //6.使用手机的功能
        //我们通过 . 来调用功能,类似于中文的 "的"

        p2.video();
        p2.message();
        p2.call();

        p.call();
        p.message();
        p.video();

        System.out.println(p.brand);
        System.out.println(p.price);
        System.out.println(p.size);
        System.out.println(p.color);

        //修改成员变量的值
        p2.brand = "小米";
        p2.color = "red";
        p2.price = 9999.99;
        p2.size = 6.6;
        System.out.println(p2.brand);
        System.out.println(p2.color);
        System.out.println(p2.price);
        System.out.println(p2.size);
    }
}

//1.通过class关键字创建一个手机类--描述手机这一类事物
class Phone{
    //2.特征--类的成员变量来描述--位置:类里方法外
    String brand;
    double price;
    double size;
    String color;

    //3.功能--类的方法来描述--修饰符 返回值类型 方法名(参数列表){方法体}
    public void call(){
        System.out.println("正在打电话");
    }
    public void message(){
        System.out.println("正在发短信");
    }
    public void video(){
        System.out.println("正在看直播");
    }
}












