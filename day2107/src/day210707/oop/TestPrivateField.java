package day210707.oop;

/**
 * 关于 set/get方法
 *
 *
 */
public class TestPrivateField {
    public static void main(String[] args) {
        Cat cat = new Cat();
        cat.setName("小黑");
        String name = cat.getName();
        System.out.println(name);
    }
}

class Cat{
    private String name;

    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }
}
