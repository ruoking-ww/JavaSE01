package day210706.basic02;

import org.junit.Test;

/**
 * 本类用于测试取余运算符 和 自增自减
 *
 *  自增自减
 *  前缀式: ++a  --a   先运算再使用
 * 	后缀式: a++  a--   先使用再运算
 *
 *  自增自减运算符会改变变量本身的值
 *  普通的四则运算只能改变算式本身的值
 *
 *
 *
 */
public class TestOperator {
	public static void main(String[] args) {
		/**
		 * 基本的四则运算符: + - * /
		 */
		System.out.println(25/10); // int/int -> int 商是2
		System.out.println(25%10); // 余数为5
	}
	
	//练习: 获取一个两位数的十位与个位
	@Test
	public void test01(){
		int x = 59;
		System.out.println(59/10); //获取十位,包含几个10
		System.out.println(59%10); //获取个位
		
		int y = 152;
		System.out.println(152/100); //获取百位
		System.out.println((152%100)/10); //获取10位
		System.out.println(152%10); //获取个位
	}
	
	//测试自增,自减运算符
	//前缀式: ++a  --a   先运算再使用
	//后缀式: a++  a--   先使用再运算
	@Test
	public void test02(){
		int a = 1;
		System.out.println(a++); //结果1
		
		int b = 1;
		System.out.println(++b); //结果2
		
//		int c = 1;
//		System.out.println(--c);
		
		int d = 1;
		System.out.println(d--);
		
		int c = 0;
		System.out.println(--c-c-c--); 
		//打印结果为1  //-1-(-1)-(-1)
		//最后c本身的值为 -2
/**
 * 自增自减运算符会改变变量本身的值
 * 普通的四则运算只能改变算式本身的值
 */
	}
}




















