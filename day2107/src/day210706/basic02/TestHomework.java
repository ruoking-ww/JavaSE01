package day210706.basic02;

import org.junit.Test;
/**
 * 作业: 求[1,100]以内元素之和,偶数之和,偶数个数
 *
 *
 */
public class TestHomework {
	@Test
	public void test01(){
		int x = 0;
		for(int i=1;i<=100;i++){
			x = x+i;
		}
		System.out.println(x);
	}
	@Test
	public void test02(){
		int x = 0;
		for(int i=1;i<=100;i++){
			if(i%2==0)
				x = x+i;
		}
		System.out.println(x);
	}
	@Test
	public void test03(){
		int x = 0;
		for(int i=1;i<=100;i++){
			if(i%2==0)
				x++;
		}
		System.out.println(x);
	}
}
