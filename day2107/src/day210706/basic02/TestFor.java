package day210706.basic02;

import org.junit.Test;

/**
 * 本类用于测试for循环结构
 *
 *
 */
public class TestFor {

	@Test
	public void test01(){
		for(int i=0;i<5;i++){
			System.out.println("无情铁手");
		}
		
		for(int i=1;i<=10;i++){
			System.out.println(i);
		}
		
		for(int i=10;i>=1; i--){
			System.out.println(i);
		}
		
		for(int i=8;i<=8888;i=i*10+8){
			if(i<8888){
				System.out.print(i+",");
			}else{
				System.out.print(i);
			}
			
		}
	}
}
