package day210706.basic02;

import java.util.Scanner;

import org.junit.Test;

public class TestScore {

	@Test
	public void test01(){
		System.out.println("请输入你的成绩:");
		int score = new Scanner(System.in).nextInt();
		
/**
 * 为了增强程序的健壮性,我们可以对分数进行判断
 * 如果是合理范围内的分数,才判断档位,否则就结束程序
 */
		if(score<0 || score>100){
			System.out.println("输入的数据不正确!");
		}else{
			if(score>=90){
				System.out.println("A");
			}else if(score>=80){
				System.out.println("B");
			}else if(score>=70){
				System.out.println("C");
			}else if(score>=60){
				System.out.println("D");
			}else {
				System.out.println("不及格");
			}
		}
	}
}



















