package day210706.basic02;

import java.util.Scanner;

import org.junit.Test;

/**
 * 平年 闰年案例
 * 
 * 需求:接收用户输入的年份,判断是平年还是闰年
 * 判断闰年条件(满足一个条件即可):
 * 条件1: 能被4整除但不能被100整除
 * 条件2: 能被400整除
 */
public class TestYear {
	@Test
	public void test01(){
		System.out.println("请输入你要判断的年份:");
		int year = new Scanner(System.in).nextInt();
		String result = (((year%4==0)&&(year%100!=0))||year%400==0)?"闰年":"平年";
		System.out.println(year+"年是"+result);
	}
	
	//实现方案一
	@Test
	public void test02(){
		System.out.println("请输入你要判断的年份:");
		int year = new Scanner(System.in).nextInt();
		String result = "平年";
		if(year%4==0){
			if(year%100!=0){
				result="闰年";
			}
		}
		if(year%400==0){
			result="闰年";
		}
		System.out.println(year+"年是"+result);
	}
	
	//实现方案二
	@Test
	public void test03(){
		System.out.println("请输入你要判断的年份:");
		int year = new Scanner(System.in).nextInt();
		String result = "平年";
		if((year%4==0 && year%100!=0) || year%400==0){
			result="闰年";
		}
		System.out.println(year+"年是"+result);
	}
}















































