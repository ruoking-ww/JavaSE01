package day210706.basic02;

import java.util.Scanner;

import org.junit.Test;

/**
 * 本类用于测试分支结构
 * 商品打折案例
 *
 *
 */
public class TestDiscount {

	@Test
	public void test01(){
		System.out.println("请输入商品的原价:");
		double price = new Scanner(System.in).nextDouble();
		double count = price;
		if(price>=10000){
			count = price*0.1;
		}else if(price>=5000){
			count = price*0.5;
		}else if(price>=2000){
			count = price*0.9;
		}else {
			count = price;
		}
		System.out.println("您实际应该支付:"+count);
	}
}










