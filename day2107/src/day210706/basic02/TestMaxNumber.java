package day210706.basic02;

import java.util.Scanner;

import org.junit.Test;

/**
 * 本类用于练习三目运算符
 * 
 * 三目运算符: 布尔表达式 ? 值1 :值2
 * 表达式为true,取值1,否则取值2
 *
 *
 */
public class TestMaxNumber {

	//接收用户输入的两个数,输出大的值
	@Test
	public void test01(){
		System.out.println("请输入你要比较的第一个整数:");
		int a = new Scanner(System.in).nextInt();
		System.out.println("请输入你要比较的第二个整数:");
		int b = new Scanner(System.in).nextInt();
		System.out.println("两个数的大值为:"+(a > b ? a : b));
	}
	
	@Test
	public void test02(){
		int a = 1,b = 2, c = 3;
		int max = a > b ? a : b;
		max = max > c ? max : c;
		System.out.println(max);
	}
}

















