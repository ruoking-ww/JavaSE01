package day210706.basic02;

import org.junit.Test;

/**
 * 本类用于练习switch结构
 */
public class TestSwitch {

	@Test
	public void test01(){
		int a = 3;
		switch(a){
		case 1: 
			System.out.println(1);
			break;
		case 2: 
			System.out.println(2);
			break;
		case 3: 
			System.out.println(3);
			break;
		case 4: 
			System.out.println(4);
			break;
		case 5: 
			System.out.println(5);
			break;
		default: 
			System.out.println(0);
		}
	}
	
	@Test
	public void test02(){
		String s = "5";
		switch(s){
			case "1":
				System.out.println("吃额");
				break;
			case "2":
				System.out.println("吃啊");
				break;
			case "3":
				System.out.println("吃吧");
				break;
			case "4":
				System.out.println("吃完");
				break;
			case "5":
				System.out.println("吃符");
				break;
			case "6":
				System.out.println("吃个");
				break;
			case "7":
				System.out.println("吃了");
				break;
			default:
				System.out.println("吃没了");
		}
	}
}





















