package day210706.basic01;

import java.util.Scanner;

/**
 * 本类用于求圆的面积
 * 
 *  π*r*r
 */
public class TestCircleArea {
	public static void main(String[] args) {
		System.out.println("请输入你要计算的半径值:");
		//定义一个浮点型的变量来保存圆的半径值
		int r = new Scanner(System.in).nextInt();
		System.out.println("圆的面积为:"+ 3.14 * r * r);
	}
}




















