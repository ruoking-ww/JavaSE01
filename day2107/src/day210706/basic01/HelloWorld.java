package day210706.basic01;
/**
 * 这是注释:注释是不会影响代码的执行,是给人看的
 */

/*
 * public	公共的,作为类的修饰符,修饰谁可以访问这个类
 * class  类--关键字,定义一个类
 * HelloWorld  类名,驼峰命名法,每个单词的首字母都大写
 * {代码..}  花括号表示类的主体,其中包含我们写的代码
 * {}[] () "" 都是成对出现的, 都是英文符号
 */
public class HelloWorld {
	
/* public static void main(String[] args){} //入口函数
 * 
 * public  --公共的,作为方法的修饰符,修饰谁可以访问这个方法
 * static --静态的,修饰这个方法为静态
 * void  --空,无, 表示这个方法没有返回值               
 * main  --表示方法的名字
 * ()  --代表这是一个方法
 * String[] args --表示方法的参数
 * {代码..} --花括号表示方法的主体,其中包含我们写的代码
 */
	public static void main(String[] args) {
		
		/*
		 * System --系统,发出系统指令
		 * out --向外输出
		 * println() --打印的方法--打印后换行
		 * "HelloWorld" --打印的内容
		 * ;  --结束符
		 */
		System.out.println("HelloWorld");
	}
}
