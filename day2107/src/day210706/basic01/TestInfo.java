package day210706.basic01;
/**
 * 本类用于个人信息输出案例
 * 
 * 需求: 在控制台打印: java架构师今年20岁，月薪100000.99，感慨java是世界上最好的语言
 */
public class TestInfo {
	//创建程序的入口函数main
	public static void main(String[] args) {
		//定义变量, 格式: 类型 变量名 = 值
		String name = "泡泡";
		int age = 18;
		double salary = 100000.999;
		System.out.println("java架构师"+name+"今年"+age+"岁，月薪"+salary+","
				+ "感慨java是世界上最好的语言");
	}
}
