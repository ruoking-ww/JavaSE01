package day210706.basic01;
/**
 * 本类用于测试 注释的 三种方式
 */
public class Demo {
	public static void main(String[] args) {
		//我是一个单行注释
		/*我是一个多行注释*/
		
		/**
		 * 我是一个文档注释
		 */
		
		int age = 18;
		String s = "hello";
		System.out.println("age");
		System.out.println(age);
		System.out.println(s);
	}
}
