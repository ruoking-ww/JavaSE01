package day210706.basic01;

import org.junit.Test;

/**
 * 本类用于查看基本类型的最大值和最小值
 */
public class TestTypeScope {
	public static void main(String[] args) {
		/*
		 * 测试整数类型 byte,short,int,long
		 * 
		 * Java提供了基本类型对应的包装类
		 */
		byte byteMin = Byte.MIN_VALUE;
		byte byteMax = Byte.MAX_VALUE;
		
		//快捷键: Ctrl+Alt+向下键  --快速向下复制
		System.out.println("byte类型的最小值为:"+byteMin);
		System.out.println("byte类型的最大值为:"+byteMax);
		
		short sMin = Short.MIN_VALUE;
		short sMax = Short.MAX_VALUE;
		System.out.println("short类型的最小值为:"+sMin);
		System.out.println("short类型的最大值为:"+sMax);
		
		int intMin = Integer.MIN_VALUE;
		int intMax = Integer.MAX_VALUE;
		System.out.println("int类型的最小值为:"+intMin);
		System.out.println("int类型的最大值为:"+intMax);
		
		long longMin = Long.MIN_VALUE;
		long longMax = Long.MAX_VALUE;
		System.out.println("long类型的最小值为:"+longMin);
		System.out.println("long类型的最大值为:"+longMax);
		
		float floatMin = Float.MIN_VALUE;
		float floatMax = Float.MAX_VALUE;
		System.out.println("float类型的最小值为:"+floatMin);
		System.out.println("float类型的最大值为:"+floatMax);
	}
	
	@Test
	public void test01(){
		float a = 0.0000000000000001f;
		System.out.println(a);
	}
}

















