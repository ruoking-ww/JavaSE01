package day210706.basic01;
/*
 * 本类用于测试计算规则
 */
public class Demo2 {
	public static void main(String[] args) {
		System.out.println(3/2);  //1    int/int -->int
		System.out.println(3/2d); //1.5  int/double -->double
		
		byte b1 = 1;
		byte b2 = 2;
		byte b3 = (byte)(b1+b2);
		System.out.println(b3);
		
		float f1 = 1.1f;
		float f2 = 1.2f;
		float f3 = f1+f2;
		System.out.println(f3);
		
		System.out.println(300000000L*60*60*24*365);
	}
}
