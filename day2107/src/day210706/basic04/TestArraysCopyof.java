package day210706.basic04;

import java.util.Arrays;

import org.junit.Test;

/**
 * 本类用于测试数组工具类的copyOf()方法
 *
 *
 */
public class TestArraysCopyof {
	@Test
	public void test01(){
		//创建一个原数组
		int[] from = {1,2,3,4,5};
//copyOf()方法用于数组的复制,有两个(或三个)参数:
//第一个参数: 原数组的名字
//第二个参数: 新数组的长度
//本方法不会修改原数组,创建的是一个新数组
		
		//普通的 复制数组
		int[] to = Arrays.copyOf(from, 5);
		//打印复制好的新数组
		System.out.println(Arrays.toString(to));
		
//数组的缩容--缩小容量
//缩容:先创建长度为3的新数组,然后按照个数从from数组中
//复制数据,类似于截取
		int[] to2 = Arrays.copyOf(from, 3);
		System.out.println(Arrays.toString(to2));
//数组的扩容: 先创建好指定长度的数组,复制原数组中的数据,不足的位置
//还是对应类型的默认值,此处int 是0 
		int[] to3 = Arrays.copyOf(from, 10);
		System.out.println(Arrays.toString(to3));

//#############################################
//我们还可以指定首尾复制数组(截取一部分复制数组)
//注意!! 这里2和4指的是被复制数组的角标, 含头不含尾[2,4)
//#############################################
		int[] to4 = Arrays.copyOfRange(from, 2, 4);
		System.out.println(Arrays.toString(to4)); // [3, 4]
	}
}

















