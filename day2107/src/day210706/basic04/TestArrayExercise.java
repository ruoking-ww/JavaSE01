package day210706.basic04;

import java.util.Arrays;
import java.util.Random;

import org.junit.Test;

/**
 * 本类用于练习数组的遍历
 */
public class TestArrayExercise {

	@Test
	public void test01(){
//		m1();
//		m2();
		m3();
	}

	//需求三: 创建一个随机数组,数组中的元素是[1,100)以内 的整数
	private void m3() {
		//动态创建一个数组
		int[] a = new int[8];
		//遍历创建好的数组
		for(int i=0; i<a.length; i++){
			a[i] = new Random().nextInt(100);
		}
		System.out.println(Arrays.toString(a));
	}

	//需求二: 动态的向数组中存入数据1~10
	private void m2() {
		//创建一个数组--动态形式
		int[] a = new int[10];
		//动态的向数组中赋值--遍历数组
		//通过for循环遍历,循环变量是数组的 下标
		for(int i=0; i<a.length; i++){
			a[i] = i + 1;  //给数组指定位置处赋值
		}
		//循环结束以后查看赋值的情况
		//注意此处需要使用数组的工具类打印
		System.out.println(Arrays.toString(a));
	}

	private void m1() {
		//定义一个数组,保存12个月份的天数
		int[] a = {31,28,31,30,31,30,31,31,30,31,30,31};
		//数组的遍历
		//我们通过下标来操作数组中的元素 [ 0 ~ 数组名.length-1 ]
		for(int i=0; i<a.length; i++){
			System.out.println((i+1)+"月有"+a[i]+"天");
		}
	}
}












