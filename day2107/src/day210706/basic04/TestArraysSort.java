package day210706.basic04;

import java.util.Arrays;

/**
 * 本类用于测试数组工具类Arrays的方法: sort()
 */
public class TestArraysSort {
	public static void main(String[] args) {
		//1, 创建一个无序的数组
		int[] a = {33,22,11,2};
		//2, 使用sort方法直接给数组中的元素排序
		/**Arrays.sort(数组名) 用于直接给数组中的元素排序,
		 * 影响的是传入的这个数组,直接更改这个数组中元素的顺序*/
		Arrays.sort(a);
		System.out.println(Arrays.toString(a));
	}
}
