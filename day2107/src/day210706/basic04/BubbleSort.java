package day210706.basic04;

import java.util.Arrays;

/**
 * 本类用于实现冒泡排序
 */
public class BubbleSort {
	public static void main(String[] args) {
	
		//1,创建一个无序的数组
		int[] a = {27,96,73,25,21};
		//2,创建一个用来排序的方法
		sortArray(a);
	}

	//创建一个实现冒泡排序的方法
	private static void sortArray(int[] a) {
		//1.外层循环--控制的是轮数--这里的轮数是 元素的个数-1
		for(int i=1; i<=a.length-1; i++){
			//2.内层循环--控制的是在这一轮中比较的次数
			/**i代表的是轮数,从1开始
			 * j代表的是数组的下标*/
			for(int j=0; j<a.length-i; j++){
				//相邻比较,位置不对就互换
				if(a[j] > a[j+1]){
					//交换数据
					int t = a[j];
					a[j] = a[j+1];
					a[j+1] = t;
				}
			}
		}
		System.out.println(Arrays.toString(a));
	}
}
