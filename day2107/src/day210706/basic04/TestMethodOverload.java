package day210706.basic04;
/**
 * 本类用于测试方法的 重载
 *
 *
 */
public class TestMethodOverload {
	/**方法定义的格式: 修饰符 返回值类型 方法名(参数列表){方法体}*/
	/**我们根据 方法名+参数列表 来确定调用哪个方法*/
	
	/**方法的重载 --- 一种现象
	 * 在同一个类中,存在方法名相同,但是参数列表不同的方法
	 * 注意查看的是对应位置上参数的 类型,与参数名无关
	 * 
	 * 当参数个数不同时,一定构成重载
	 * 当参数个数相同时,可能构成重载,具体要看参数的类型*/
	public static void main(String[] args) {
		method(); //调用无参方法method
		method(3);
		method("泡泡",3);
	}
	
	private static void method(String string, int i) {
		System.out.println(string+"今年"+i+"岁了");
	}

	private static void method(int i) {
		System.out.println("我有参数:"+i);
	}

	//创建一个无参的method方法
	private static void method() {
		System.out.println("我没有参数!");
	}
	
}
