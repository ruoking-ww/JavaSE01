package day210706.basic04;

import java.util.Arrays;

import org.junit.Test;

/**
 * 本类用于练习数组的创建和使用
 *
 * 关于数组的打印,char类型在底层做了处理,可以直接打印数组中的具体元素
 * 但是除了char类型以外的所有数组都不可以直接打印数组中的具体内容
 * 如果想查看数组中的具体元素,需要使用工具类Arrays
 * Arrays.toString(数组名)
 *
 * 数组名.length 查看数组的长度--数组中存放元素的个数
 * 数组一旦创建,长度不可改变
 *
 */
public class TestArray {

	//创建一个数组,并向数组中存入数据 hello
	@Test
	public void test01(){
		//静态创建
		char[] c1 = {'h','e','l','l','o'};
		
		char[] c2 = new char[]{'h','e','l','l','o'};
		
		//动态创建
		char[] c3 = new char[5];
		//我们通过数组的 下标来操作数组中的元素
		//数组的下标从零开始,最大下标等于数组的长度-1
		c3[0] = 'h';
		c3[1] = 'e';
		c3[2] = 'l';
		c3[3] = 'l';
		c3[4] = 'o';
		
		System.out.println(c1);  //hello
		System.out.println(c2);
		System.out.println(c3);
	}
	
	@Test
	public void test02(){
		
		int[] a = new int[2];
		System.out.println(a);  //[I@4459eb14
		
		//创建一个String类型的数组
		String[] s = {"a","b","c"};

		System.out.println(s); //[Ljava.lang.String;@5a2e4553
		System.out.println(Arrays.toString(s));
		
/**
 * 数组名.length 查看数组的长度--数组中存放元素的个数
 * 数组一旦创建,长度不可改变
 */
		System.out.println(s.length);
	}
}























