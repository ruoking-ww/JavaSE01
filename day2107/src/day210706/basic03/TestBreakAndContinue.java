package day210706.basic03;

import java.util.Scanner;

import org.junit.Test;

/**
 * 本类用于测试 break与continue
 *
 *
 */
//案例: 找数字88,让用户猜100次,如果不是88就继续猜,是88就结束
public class TestBreakAndContinue {

	@Test
	public void test01(){
		for(int i=1; i<=100; i++){
			
			System.out.println("请输入你要猜的整数:");
			int input = new Scanner(System.in).nextInt();
			//如果if语句只有一句,大括号可以省略不写
			//continue 效果是跳过其后的循环体,直接进行下一轮循环
			if(input != 88){
				continue;
//				break和continue之后都不允许有代码,因为是不可到达的代码
			}
				
			if(input == 88){
				System.out.println("恭喜你,猜对了!");
				break;
			}
		}
	}
}
