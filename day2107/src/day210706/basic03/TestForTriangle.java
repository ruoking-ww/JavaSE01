package day210706.basic03;

import org.junit.Test;

/**
 * 利用for循环打印一个左直角三角形
 *   *
 *   * *
 *   * * *
 *   * * * *
 *   * * * * *
 *   * * * * * * 
 */
public class TestForTriangle {

	@Test
	public void test01(){
		//用已知, 释新知
		//1.先完成一个6*6的矩形
		for(int i=1; i<=6; i++){
			
			/**此处列数要随着行数而变化,列数的最大值就是行数*/
			for(int j=1; j<=i; j++){
				System.out.print("* ");
			}
			System.out.println(); //换行
		}
	}
	
	//本方法用于测试99乘法表
	@Test
	public void test99Excel(){
		for(int i=1; i<=9; i++){
			for(int j=1; j<=i; j++){
				
				// "\t" 表示制表符
				System.out.print(j+"*"+i+"="+j*i+"\t");
			}
			System.out.println();
		}
	}
}






















