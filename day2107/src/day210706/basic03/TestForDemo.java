package day210706.basic03;
/**
 * 本类用作for循环的入门案例
 */
public class TestForDemo {
	public static void main(String[] args) {
		
/**
 * 外层循环执行一次,内层循环执行多次
 * 
 * 外层循环控制的是轮数,内层循环控制的是每轮执行的次数
 */
		for(int i=1; i<=3; i++){
			System.out.println(i);
			
			for(int j=1; j<=5; j++){
				System.out.println(j); 
			}
		}
	}
}



