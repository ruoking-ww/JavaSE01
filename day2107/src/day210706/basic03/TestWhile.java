package day210706.basic03;

import java.util.Random;
import java.util.Scanner;

import org.junit.Test;

/**
 * 本类用于练习while结构
 *
 *
 *
 */
public class TestWhile {

	//生成一个随机数让用户去猜
	@Test
	public void test01(){

//		参数100是自定义的,此时生成的随机数范围是[0,100)以内的整数
		int random = new Random().nextInt(100);
//		System.out.println(random);
		
		while(true){  //死循环
			//注意:死循环一定要设置出口
			
			//提示并接收用户输入的数字
			System.out.println("请输入你要猜的数:");
			int input = new Scanner(System.in).nextInt();
			
			if(input > random){
				System.out.println("猜大了,小点小点");
			}else if(input < random){
				System.out.println("猜小了,往大点猜");
			}else if(input == random){
				System.out.println("恭喜你,猜对了!");
				break;
			}
		}
	}
	
	/**
	 * 本方法用于练习do while结构
	 */
	@Test
	public void test02(){
		
		int random = new Random().nextInt(100);
		do{
			//提示并接收用户猜的数字
			System.out.println("猜猜看");
			int input = new Scanner(System.in).nextInt();
			//将随机数与用户输入的数字作比较
			if(input>random){
				System.out.println("猜大了");
			}else if(input<random){
				System.out.println("猜小了");
			}else if(input==random){
				System.out.println("猜对了");
				break;
			}
		}while(true);
	}
}























