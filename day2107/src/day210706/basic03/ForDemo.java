package day210706.basic03;

import org.junit.Test;

/**
 * 本类用于测试 for循环
 */
public class ForDemo {
	
	@Test
	public void test01(){
		/*
		 * 需求:求出1-100以内所有偶数的个数
		 */
		int count = 0;
		for(int i=1; i<=100; i++){
			if(i % 2 ==0){
				count++;
			}
		}
		System.out.println("1-100偶数的个数是"+count);
	}
	
//	打印一个由*组成的矩形
	@Test
	public void test02(){
		for(int i=1; i<=3; i++){ //外层循环
			for(int j=1; j<=5; j++){ //内层循环
				System.out.print("*");
			}
			System.out.println(); //换行
		}
	}
}


























