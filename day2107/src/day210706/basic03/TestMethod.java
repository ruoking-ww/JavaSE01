package day210706.basic03;


import org.junit.Test;

/**
 * 本类用作方法的入门案例
 *
 *
 */
public class TestMethod {
	
	//我们使用方法名+参数列表的方式来使用方法的功能
	@Test
	public void test01(){
		f1();
		f2(3);
	}

	//这个是用来测试方法的参数
	private void f2(int age) {
		System.out.println("我今年"+age+"岁啦");
	}

	//修饰符  返回值类型  方法名(参数列表){方法体;}
	private void f1() {
		System.out.println("哈喽!");
		
		
	}
}





























