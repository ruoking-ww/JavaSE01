package day210706.basic03;
/**
 * 本类用于测试各种类型变量的默认值
 *
 *
 */
public class TestVariable {
	
	static byte a;
	static short b;
	static int c;
	static long d;
	static float e;
	static double f;
	static char g;
	static boolean h;
	static String i;
	
	/**
	 * 静态只能使用静态成员
	 */
	public static void main(String[] args) {
		System.out.println(a);
		System.out.println(b);
		System.out.println(c);
		System.out.println(d);
		System.out.println(e);
		System.out.println(f);
		System.out.println(g);
		System.out.println(h);
		System.out.println(i);
	}
}
