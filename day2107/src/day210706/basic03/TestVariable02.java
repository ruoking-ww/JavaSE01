package day210706.basic03;
/**
 * 本类用于测试 成员变量和 局部变量
 *
 * 成员变量
 * 	 1.位置: 类里,方法外
 *   2.注意: 可以不手动初始化,会自动赋予对应类型的默认值
 *   3.作用域: 在整个类里都生效,类消失,变量才会消失
 *
 * 局部变量
 *   1.位置: 方法里/局部代码块里
 *   2.注意: `使用时`必须手动初始化
 *   3.作用域: 只在声明的方法或局部代码块中有效.当方法执行完毕,变量就释放
 *
 * 变量的就近原则: 当成员变量与局部变量同名时,打印近的局部变量
 *
 */
public class TestVariable02 {

	//  成员变量
	static int count;
	static int sum;
	
	public static void main(String[] args) {

		//局部变量
		int sum = 100;
		
		//变量的就近原则: 当成员变量与局部变量同名时,打印近的局部变量
		System.out.println(sum);
		System.out.println(count);
	}
	
	public void test01(){
		int i = 0;
		//for(int i=2; i<10; i++){  //报错:Duplicate local variable i
		//局部变量不可以重复声明, 注意:是声明!
		for(i=2; i<10; i++){
			System.out.println(i);
		}
	}
	
	public void test02(){
		//关于局部变量的声明和作用域! 注意和上面方法的区分 
		for(int i=0; i<10; i++){
			System.out.println(i);
		}
		int i = 2; 
		System.out.println(i);
	}
}


























