package day210721.aboutThread;

/**
 * 多线程实现方式一
 *   继承Thread类
 *
 *  线程的随机性: 多个线程对象执行的效果是不可控的,因为CPU会调度处理
 *  结果具有随机性,至于哪个时间片让哪个线程执行,时间片有多长,我们都控制不了.
 *
 */
public class TestThread {
    public static void main(String[] args) {
        //MyThread t = new MyThread();
        //MyThread t2 = new MyThread();
        //MyThread t3 = new MyThread();
        MyThread t4 = new MyThread();
        /*start方法才会把线程加入就绪队列,以多线程的方式运行.运行的代码写在run方法里*/
        //t.start();
        //t2.start();
        //t3.start();
        System.out.println(t4);
        t4.start();
    }
}


class MyThread extends Thread{

    public MyThread() {
    }

    public MyThread(String name) {
        /**注意:
         * 子类继承父类,子类构造器一定会有一句super语句
         * [如果没有手动写明,编译器会自动添加super空参指向父类构造器],
         * 因为父类构造器可能会有多个(重载构造器),所以这里可以使用
         * super(参数)来指定想要调用的父类构造器,以实现某些功能
         * (重载的构造器里面会有一些不同的代码内容实现)*/
        super(name);
    }

    /*线程中的业务必须写在run方法里面*/
    @Override
    public void run() {
        //输出10次当前正在执行的线程名称
        for (int i = 0; i < 100; i++) {
            //Thread类的getName方法可以获取当前正在执行的线程名称
            System.out.println("线程名称是:"+getName());
        }
    }
}
