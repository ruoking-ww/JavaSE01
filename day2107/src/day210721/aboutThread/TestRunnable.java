package day210721.aboutThread;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * 多线程实现方式二
 *
 *  实现Runnable接口
 *
 */
public class TestRunnable {
    public static void main(String[] args) {
        MyRunnable target = new MyRunnable();

        //Thread t1 = new Thread(target,"线程x");
        //Thread t2 = new Thread(target,"线程z");
        //Thread t3 = new Thread(target,"线程y");
        //
        //t1.start();
        //t2.start();
        //t3.start();

        //创建线程池 //使用工具类: Executors
        //调用newFixedThreadPool(线程数) 方法来创建出指定线程数的线程池
        //线程池的类型是 ExecutorService
        ExecutorService pool = Executors.newFixedThreadPool(5);
        for (int i = 0; i < 5; i++) {
            pool.execute(target);
        }
    }
}

class MyRunnable implements Runnable{

    //把业务放入run方法,
    @Override
    public void run() {
        for (int i = 0; i < 50; i++) {
            /*Thread.currentThread(),获取当前正在执行的线程对象*/
            System.out.println("线程名是:"+Thread.currentThread().getName());
        }
    }
}