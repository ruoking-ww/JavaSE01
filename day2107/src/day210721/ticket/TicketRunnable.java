package day210721.ticket;

/**
 * 实现Runnable接口的方式 做售票案例
 *
 *
 * 为什么会出现00
 */
public class TicketRunnable {

    public static void main(String[] args) {

        TicketR target = new TicketR();

        Thread t1 = new Thread(target);
        Thread t2 = new Thread(target);
        Thread t3 = new Thread(target);
        Thread t4 = new Thread(target);

        t1.start();
        t2.start();
        t3.start();
        t4.start();
    }
}

class TicketR implements Runnable{

    int tickets = 100;

    @Override
    public void run() {
        while (true){
            System.out.println("线程:"+Thread.currentThread().getName()+"票数为:"+tickets--);

            if (tickets<=0)
                break;
        }
    }
}
