package day210721.ticket;

/**
 * 本类用来解决实现接口方式的售票安全隐患问题
 *
 */
public class TicketRunnable2 {

    public static void main(String[] args) {

        TicketR2 target = new TicketR2();

        Thread t1 = new Thread(target);
        Thread t2 = new Thread(target);
        Thread t3 = new Thread(target);
        Thread t4 = new Thread(target);

        t1.start();
        t2.start();
        t3.start();
        t4.start();
    }
}

class TicketR2 implements Runnable{

    int tickets = 100;
    Object o = new Object();

    @Override
    public void run() {
        while (true){

            //定义一个同步代码块,注意锁对象需要唯一
            synchronized (o){
                if (tickets>0){
                    try {
                        Thread.sleep(100);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    System.out.println("线程:"+Thread.currentThread().getName()+"票数为:"+tickets--);
                }

                if (tickets<=0)
                    break;
            }


        }
    }
}
