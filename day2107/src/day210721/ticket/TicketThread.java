package day210721.ticket;
/**
 * 本类通过继承的方式实现多线程 售票案例
 *
 * 需求:4个窗口,模拟售票,同时售票共计100张,售完即止
 *
 */
public class TicketThread {
    public static void main(String[] args) {

        TickeT t1 = new TickeT();
        TickeT t2 = new TickeT();
        TickeT t3 = new TickeT();
        TickeT t4 = new TickeT();

        t1.start();
        t2.start();
        t3.start();
        t4.start();
    }
}

//自定义多线程类--完成售票业务
class TickeT extends Thread{

    //int tickets = 100; //共计100张票

    //将票数变为静态资源,被本类的所有对象共享
    static int tickets = 100;

    @Override
    public void run() {
        while (true){

            /*手动添加休眠,暴露出多线程可能存在的问题
             * 问题一: 重卖, 同一张票卖了多次
             * 问题二: 超卖, 卖的数量超出了实际值*/
            try {
                Thread.sleep(300);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

            System.out.println("线程"+getName()+"票数还有"+tickets--);

            if (tickets <= 0)
                break;
        }

    }
}
