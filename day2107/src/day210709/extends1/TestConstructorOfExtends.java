package day210709.extends1;

/**
 * 本类用于测试 ***继承*** 中构造方法的使用
 * +++++++++++++++++++++++++++++++++++++++++++++++++++++++
 * 1.子类创建对象时,会先调用父类的构造方法.
 *   子类构造方法中的第一行 默认存在 super(); --它用来调用父类的无参构造方法
 * 2.如果父类没有默认的无参构造方法,有含参构造方法,
 *   那么子类需要通过 super(参数); 来指定调用父类的含参构造方法
 * 3.构造方法不可以被继承!由于构造方法的语法限制.
 *   构造方法要求方法名是本类类名
 * +++++++++++++++++++++++++++++++++++++++++++++++++++++++
 *
 *
 */
public class TestConstructorOfExtends {
    public static void main(String[] args) {

        //由于存在默认的无参构造,所以可以直接创建对象
        Father father = new Father();
        Son son = new Son();
        Son son2 = new Son("大头儿子");
    }
}

//创建一个父类
class Father{
    public Father(){
        System.out.println("父类的无参构造");
    }
    public Father(int n){
        System.out.println("父类的含参构造"+n);
    }
}

//创建一个子类
class Son extends Father{
    public Son(){
        super(1);
        System.out.println("子类的无参构造");
    }
    public Son(String s){
        System.out.println("子类的含参构造"+s);
    }
}