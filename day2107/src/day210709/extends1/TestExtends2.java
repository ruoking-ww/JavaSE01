package day210709.extends1;

/**
 * 本类用于测试 继承中方法的使用
 *
 *
 * 方法的重写: 继承之后,子类对父类的方法不满意,可以重写
 * 子类可以拥有自己的特有功能--新功能
 * ++++++++++++++++++++++++++++++++++++++++++++++++++++
 * 方法重写的规则: 两同两小一大原则
 * (方法签名,返回值类型,抛出异常类型,修饰符) [注意:联系实际情况去理解]
 *
 *  -- 两同: 方法名&参数列表保持一致 --或者说 方法签名保持一致
 *  -- 两小: 子类的返回值类型要 <= 父类的返回值类型
 *           子类方法抛出的异常类型要 <= 父类方法抛出的异常类型
 *  -- 一大: 子类方法的修饰符要 >= 父类方法的修饰符
 *
 * 私有方法不会被重写,因为父类的私有方法和属性 对子类不可见
 * +++++++++++++++++++++++++++++++++++++++++++++++++++++++++
 * OCP原则(Open Closed Principle 开闭原则):
 *      面向功能修改关闭, 面向功能拓展开放
 *      比如重写: 在不修改父类方法的前提下,实现功能的更新
 *
 *
 *
 *
 *
 */
public class TestExtends2 {
    public static void main(String[] args) {
        Son2 s = new Son2();
        s.eat();
    }
}
//创建父类
class Father2{
    public void eat(){
        System.out.println("爸爸爱吃肉");
    }

    public void play(){
        System.out.println("爱放风筝~");
    }
}
//创建子类
class Son2 extends Father2{
    /**重写父类的方法 eat() */
    //注解: 就是一个 小标签/标记, 用于标记这是一个重写的方法
    @Override
    public void eat(){
        System.out.println("儿子爱吃蔬菜");
    }
    public void work(){
        System.out.println("我们都是圣斗士");
    }
}
