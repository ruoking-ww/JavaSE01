package day210709.oopAboutStatic;

/**
 * 本类用于测试静态的调用关系
 *
 * 1.普通方法能否调用 静态资源? -- 可以!!!
 *
 * 2.静态方法能否调用 普通资源? --不可以!!!
 *
 * 3.静态方法能否调用 静态资源? --可以!!!
 *
 * 总结: 静态方法只能调用静态资源
 *       非静态方法可以调用静态资源 和 非静态资源
 *
 *
 *
 *
 *
 */
public class TestStatic02 {
}

//创建一个老师类
class Teacher{
    //定义普通属性
    String name;

    //定义普通方法
    public void teach(){
        System.out.println("空尼几哇");

        System.out.println(age);
        eat();
    }

    //定义静态属性
    static int age;

    //定义静态方法
    public static void eat(){
        System.out.println("吃了睡,睡了吃");
    }
    public static void sleep(){
        System.out.println("睡觉");
    }

}









