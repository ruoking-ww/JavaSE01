package day210709.oopAboutStatic;

/**
 * 本类用作static的入门案例
 *
 * static 静态可以把普通资源修饰成静态资源
 * 可以用来修饰成员变量与方法,一般写在修饰符之后
 *
 * (对象可以调用静态属性和方法)
 * 我们发现 IDEA不提示 通过对象调用静态属性和方法
 *
 * +++++++++++++++++++++++++++++++++++++
 * 静态资源属于类资源,可以被类名直接调用
 * 静态资源是优先于对象进行加载的,随着类的加载而加载(静态资源就像开机自启项)
 * 比对象先加载进入内存,即使没有对象也可以通过类名直接调用
 *
 * 静态资源被全局所有对象 共享,只有一份
 * +++++++++++++++++++++++++++++++++++++
 *
 *
 *
 *
 *
 */
public class TestStatic01 {
    public static void main(String[] args) {

        System.out.println(Student.name);
        Student.study();

        //main函数中创建对象进行测试
        Student student = new Student();
        Student student2 = new Student();

        student.name = "阿猫";
        System.out.println(student.name);    //阿猫
        System.out.println(student2.name);   //阿猫

        student2.name = "阿狗";
        System.out.println(student.name);    //阿狗
        System.out.println(student2.name);   //阿狗
        //System.out.println(student.name);
        //student.study();
    }
}

//创建学生类
class Student{
    //定义属性
    //用static将一个普通属性修饰成为静态属性
    static String name;
    int number;

    //定义一个静态方法
    public static void study(){
        System.out.println("好好学习,天天向上");
    }
    public void speak(){
        System.out.println("爱是一道光,疑是地上霜");
    }
}












