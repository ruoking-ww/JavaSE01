package day210709.oopAboutStatic;

/**
 * 本类用于测试 三种代码块 与 它们的执行顺序
 *
 * ++++++++++++++++++++++++++++++++++++++++++++++++
 * 静态代码块
 *    位置: 类里 方法外
 *    执行时机: 静态代码块属于类资源,随着类的加载而加载,优先于对象加载,只加载一次
 *    作用: 用于加载一些需要第一时间就加载,并只加载一次的 资源
 *
 * 执行顺序:
 *    静态代码块->构造代码块->构造方法->局部代码块
 * ++++++++++++++++++++++++++++++++++++++++++++++++
 *
 *
 */
public class TestStaticBlock {
    public static void main(String[] args) {

        Pig pig = new Pig();
        Pig pig2 = new Pig();
        Pig pig3 = new Pig();

        pig.eat();
    }
}

//创建一个小猪类
class Pig{

    //创建静态代码块
    static{
        System.out.println("我是静态代码块");
    }

    //创建构造代码块
    {
        System.out.println("我是构造代码块");
    }

    //创建构造方法
    public Pig(){
        System.out.println("我是无参构造方法");
    }

    //创建普通方法
    public void eat(){
        System.out.println("我是普通方法,小猪爱吃菜叶子");

        //创建局部代码块
        {
            System.out.println("我是局部代码块");
        }
    }


}











