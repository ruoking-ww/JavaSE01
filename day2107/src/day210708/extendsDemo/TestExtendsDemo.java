package day210708.extendsDemo;

/**
 * 本类测试 ###继承### 的基础使用
 *
 * 继承的特点
 *    使用extends关键字来表示继承关系
 *    相当于子类把父类的功能复制了一份
 *    Java只支持单继承
 *    继承可以传递(爷爷/儿子/孙子这样的关系)
 *    不能继承父类的私有成员
 *    继承多用于功能的修改,子类可以在拥有父类功能的同时,进行功能拓展
 *    像是is a的关系 (依赖性强,耦合性强)
 *  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++
 *     子类不可以使用父类的私有资源, 父类的构造方法不可以被继承
 *     注意: 继承相当于子类把父类的资源复制了一份,也包含私有资源,
 *     只是父类的私有资源对子类不可见,(后期可通过反射获取私有资源)
 *  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++
 *  1.子类创建对象时,会先调用父类的构造方法.
 *    子类构造方法中的第一行 默认存在 super(); --它用来调用父类的无参构造方法
 *  2.如果父类没有默认的无参构造方法,有含参构造方法,
 *    那么子类需要通过 super(参数); 来指定调用父类的含参构造方法
 *  *  *  *  *  *  *  *  *
 *  3.构造方法不可以被继承!
 *  *  *  *  *  *  *  *  *
 *    由于构造方法的语法限制.构造方法要求方法名是本类类名
 *
 *
 *
 */
public class TestExtendsDemo {
    public static void main(String[] args) {
        //创建对象进行测试
        Animal animal = new Animal();   //创建爷类
        Panda panda = new Panda();      //创建父类
        Babble babble = new Babble();   //创建子类

        //通过对象调用方法
        /**子类继承父类以后,可以使用父类的功能
         * 继承可以传递,爷的功能传给爸,爸的功能传给子*/
        animal.eat();
        panda.eat();
        babble.eat();

        babble.show();

        //私有的方法子类不可以调用
        //babble.cry();
    }
}



//创建一个爷类
class Animal{
    public void eat(){
        System.out.println("小动物都要吃吃喝喝");
    }

    //私有的方法子类不可以调用
    private void cry(){
        System.out.println("(～￣▽￣)～");
    }
}

//创建一个父类
class Panda extends Animal{
    //创建小猫类的属性
    int testNumber01 = 100;
    //权限修饰符--私有资源只能在本类访问
    private int testNumber02 = 1000;
}

//创建一个子类
class Babble extends Panda{
    /**子类是可以拥有自己的特有功能
     * 子类的特有功能,父类不能使用
     *
     * 子类不可以使用父类的私有资源
     * 注意: 继承相当于子类把父类的资源复制了一份,也包含私有资源,
     * 只是父类的私有资源对子类不可见,(后期可通过反射获取私有资源)
     *
     *
     * */
    public void show(){
        System.out.println(testNumber01);

        //父类私有的成员变量,子类不可调用
        //System.out.println(testNumber02);

        System.out.println("给你看个大宝贝");
    }
}