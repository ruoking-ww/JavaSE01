package day210708.extendsDemo;
/**
 * 本类用于测试继承的用法
 *
 * 注意 理清 父类资源和子类资源的 调用
 * 当 父类的成员变量,子类的成员变量,子类的局部变量 同名时,假设为a
 * this.a 指向本类的成员变量a, super.a指向父类的成员变量a
 *
 * 关键字super代表父类 Father super = new Father();
 * 想指定**同名的**父类成员变量时,使用super来指定
 *
 *
 */
public class TestExtends1 {
    public static void main(String[] args) {
        new Son().eat();
    }
}

//创建父类
class Father{
    int a = 10;
}

//创建子类并与父类建立继承关系
class Son extends Father{
    int a = 100;

    public void eat(){
        int a = 1000;

        System.out.println(a);
        System.out.println(this.a);
        System.out.println(super.a);
    }
}