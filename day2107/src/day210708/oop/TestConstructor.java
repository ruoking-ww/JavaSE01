package day210708.oop;

/**
 * 本类用于测试构造方法
 *
 *
 *  1.每个类中默认存在无参构造函数(方法)
 *    方法的格式: 修饰符 返回值类型 方法名(参数列表){}
 *    构造方法的格式: 修饰符 类名(参数列表){}
 *
 *    构造方法也存在重载现象: 无参构造/含参构造
 *
 *    每次创建对象时都会触发对应的构造方法来创建对象
 *
 *  2.如果手动提供了含参构造,默认的无参构造就不会自动创建
 *    如果此时需要无参构造,则手动添加
 */
public class TestConstructor {
    public static void main(String[] args) {
        //4.创建对象进行测试
        /**每次创建对象时都会触发对应的构造方法来创建对象*/
        People p = new People();

        People p2 = new People(3);

        People p3 = new People("海绵宝宝",15,"波波");

        //5.可以通过对象调用资源
        System.out.println(p.name);
        System.out.println(p.age);
        System.out.println(p.gender);

        p.eat();
    }
}

//1.创建一个People类,把人相关特征与行为封装成一个类组件
class People{
    //2.属性/字段/成员变量
    String name;
    int age;
    String gender;

    public People(){
        System.out.println("我是People类的无参构造函数");
    }

    //创建一个含参构造
    public People(int n){
        System.out.println("我是People类的含参构造" + n);
    }

    //创建People类的全参构造
    public People(String n,int a,String g){
        System.out.println("我是People类的全参构造");
        name = n;
        age = a;
        gender = g;
    }

    //3.功能/方法
    public void eat(){
        System.out.println("人是铁,饭是钢,一顿不吃饿得慌");
    }
}









