package day210708.oop;
/**作业03: 练习构造函数*/
public class TestConstructor01 {
    public static void main(String[] args) {
        People01 p = new People01();
        People01 p2 = new People01("小颖","老baby",3);
        System.out.println(p2.name);
        System.out.println(p2.gender);
        System.out.println(p2.age);
        p.eat();
        p2.eat();
    }
}

class People01{
    String name;
    String gender;
    int age;

    public People01() {
        System.out.println("我是无参构造函数");
    }
    public People01(String name, String gender, int age) {
        System.out.println("我是全参构造函数");
        this.name = name;
        this.gender = gender;
        this.age = age;
    }
    public void eat(){
        System.out.println("人是铁,饭是钢,一顿不吃饿得慌");
    }
}
