package day210708.oop;

/**
 * 本类用于测试构造代码块 和 局部代码块
 *
 *
 *
 */
public class TestBlock {
    public static void main(String[] args) {
        Teacher t = new Teacher();
        Teacher t1 = new Teacher("javase");
        t.sleep();
    }
}

//1.创建一个老师类用作测试数据
class Teacher{

    //定义成员变量,类资源
    String subject;
    String country;

    //创建构造代码块测试
    /**************************************
     * 构造代码块
     * 位置: 类里方法外
     * 执行时机: 每次创建对象时都会执行一次,且优先于构造方法执行
     * 作用: 用于提取所有构造方法的共性功能
     * ************************************/
    {
        country = "大中华";
        System.out.println("我是构造代码块");
    }

    //创建一个无参构造
    public Teacher(){
        System.out.println("无参构造"+country);
    }
    public Teacher(String subject){
        System.out.println("全参构造"+country);
        this.subject = subject;
    }

    //创建一个普通方法
    public void sleep(){

        //创建局部代码块
        /*******************************
         * 局部代码块
         * 位置: 方法里
         * 执行时机: 调用本方法时执行
         * 功能: 用于控制变量的使用范围,变量只能在局部使用,
         * 出了局部代码块就失效
         * *****************************/
        {
            int i = 10;
            System.out.println(i);
            System.out.println("我是局部代码块");
        }
        //System.out.println(i); //报错,i出了代码块就无法被使用
    }
}