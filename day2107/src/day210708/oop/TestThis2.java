package day210708.oop;

//本类用于测试构造方法间的互相调用
public class TestThis2 {
    public static void main(String[] args) {
        Bird b = new Bird();
        Bird b2 = new Bird("小白");
        /*不能按照调用普通方法的格式调用构造方法*/
        //b.Bird();
    }
}

//1.创建一个小鸟类
class Bird{
    //2.1 创建本类的无参构造
    public Bird(){
        /**在无参构造器中调用含参构造器
         * 注意: *******************************************
         * 1.构造方法的调用只能是单向的,不能来回互相调用,否则会报错
         * 2.this调用构造方法的语句必须在第一行
         * *************************************************/
        //this("小飞");
        System.out.println("无参构造");
    }
    //2.2 创建本类的含参构造
    public Bird(String s){
        /*在含参构造器中调用无参构造器的标准格式*/
        this();
        System.out.println("含参构造");
    }
}