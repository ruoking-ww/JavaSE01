package day210708.oop;

//本类用于测试<<<成员变量与局部变量同名时,this的使用情况>>>
public class TestThis {
    public static void main(String[] args) {

        Cat c = new Cat();
        c.jump();
    }
}

//创建一个Cat类
class Cat{
    //定义成员变量
    int sum = 100;
    int a = 666;

    public void jump(){
        //定义局部变量
        int  sum = 10;
        System.out.println(sum);
        System.out.println(this.sum);
        System.out.println(a);
    }
}