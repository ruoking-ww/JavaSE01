package day210708.oop;

/**
 * 本类用于测试构造方法
 */
public class TestConstructor2 {

}

//创建一个小猪类
class Pig{
    //定义小猪类的属性
    String name;
    int age;
    String food;
    //创建构造方法
    public Pig() {
    }

    /**1.变量的分类:
     * 成员变量: 类里方法外,属于类资源
     * 局部变量: 方法里,属于方法的资源
     * 2.成员变量与局部变量存在可能同名的现象
     *      变量使用时有一个就近原则,优先使用近处的局部变量
     *
     * 3.我们可以把this理解成本类的对象
     * 可以通过this指定本类的资源,指定的是类资源,成员变量*/
    public Pig(String name, int age, String food) {
        this.name = name;
        this.age = age;
        this.food = food;
    }
}
