package day210714.aboutWrapType;

import java.lang.annotation.Native;

public final class IntegerMy extends Number implements Comparable<IntegerMy> {

    @Native public static final int MIN_VALUE = 0x80000000;

    @Override
    public int compareTo(IntegerMy o) {
        return 0;
    }

    @Override
    public int intValue() {
        return 0;
    }

    @Override
    public long longValue() {
        return 0;
    }

    @Override
    public float floatValue() {
        return 0;
    }

    @Override
    public double doubleValue() {
        return 0;
    }
}
