package day210714.aboutWrapType;

/**
 * 本类用于自动装箱 和 自动拆箱
 *
 * 1. 自动装箱:编译器会自动把基本类型int 转换成包装类型 Integer
 * 其底层自动执行的代码: Integer.valueOf();
 *
 * 2. 自动拆箱:编译器会自动把 包装类型Integer 转换成基本类型 int
 * 底层执行的代码  this.intValue();
 *
 */
public class TestBox {
    public static void main(String[] args) {
        //定义包装类型的数据(之前的两种创建方式)
        Integer i1 = new Integer(127);
        Integer i2 = Integer.valueOf(127);

        //现在的方式

        Integer i3 = 127;  //不会报错,这个现象就是自动装箱

        //int i = i1.intValue();

        int i4 = i1; //不会报错,这个现象就是自动拆箱
    }
}
