package day210714.aboutWrapType;

import org.junit.Test;

/**
 * 本类用于测试包装类
 *
 * 1.Integer包装类型的默认值是null
 *
 * 2.Integer.valueOf() 这种创建方式有高效的效果
 * 如果数据在 -128~127这个范围内,相同的数据只会存储一次
 * 后续再存都是使用之前存过的数据
 *
 * 关于包装类
 * 基本类型只存值,包装类型就是对基本类型进行包装,提供了丰富的功
 *
 */
public class TestNumber {

    static Integer i1;

    public static void main(String[] args) {


        System.out.println(i1); //默认值是null

        Integer i1 = new Integer(127);
        Integer i11 = new Integer(5);
        Integer i111 = new Integer(5);

        System.out.println(i1 == i11);  //false
        System.out.println(i11 == i111); //false

        Integer i2 = Integer.valueOf(127);
        Integer i3 = Integer.valueOf(127);
        System.out.println(i1 == i2); //false
        System.out.println(i2 == i3); //true
        //Integer.valueOf() 这种创建方式有高效的效果
        //如果数据在 -128~127这个范围内,相同的数据只会存储一次
        //后续再存都是使用之前存过的数据

        Integer i4 = Integer.valueOf(300);
        Integer i5 = Integer.valueOf(300);
        System.out.println(i4 == i5);  //false
        //虽然创建方式比较高效,但数据超出了高效的范围就不好使了
    }

    //创建double类型对应的包装类
    @Test
    public void test01(){
        Double d1 = new Double(3.14);

        Double d2 = Double.valueOf(3.14);
        Double d3 = Double.valueOf(3.14);
        System.out.println(d1 == d2);  //false

        /**只有Integer的valueOf()方法才有高效的效果,其他类型没有*/
        System.out.println(d2 == d3);  //false

    }

    //测试常用方法
    @Test
    public void test02(){

        //parseInt()方法, 将字符串类型的整数 转换成 int类型
        System.out.println(Integer.parseInt("300")+10);

        //注意, 浮点类型的运算会有精度损失现象
        System.out.println(Double.parseDouble("3.14")+1); //4.140000000000001

    }
}
