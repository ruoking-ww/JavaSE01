package day210714.api;

/**
 * 本类用于测试顶级父类 Object
 *
 * hashcode方法: 返回该对象的哈希码值,用来区分不同的对象,不同的对象生成的哈希码值不同
 *
 * equals方法
 *
 *
 *
 */
public class TestObject {
    public static void main(String[] args) {
        Student s = new Student("张飞",22);
        Student s2 = new Student("张飞",22);
        System.out.println(s);
        System.out.println(s.equals(s2));
    }
}

//创建学生类用于测试
class Student{
    String name;
    int age;

    public Student() {
    }
    public Student(String name, int age) {
        this.name = name;
        this.age = age;
    }

    @Override
    public String toString() {
        return "Student{" +
                "name='" + name + '\'' +
                ", age=" + age +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Student student = (Student) o;

        if (age != student.age) return false;
        return name != null ? name.equals(student.name) : student.name == null;
    }

    @Override
    public int hashCode() {
        int result = name != null ? name.hashCode() : 0;
        result = 31 * result + age;
        return result;
    }
}