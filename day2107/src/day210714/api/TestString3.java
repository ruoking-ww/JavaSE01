package day210714.api;

/**
 * 本类用于练习字符串的拼接
 *
 *
 */
public class TestString3 {
    public static void main(String[] args) {
        //method();  //普通字符串的拼接
        method2(); //高效字符串的拼接
    }

    //用于测试普通字符串的拼接
    private static void method() {
        String str = "abcdefghijklmnopqrstuvwxyz";

        //将指定的字符拼接10000次
        String result = "";

        //我们可以给程序添加一个计时的功能
        long t1 = System.currentTimeMillis();

        for (int i=1; i<10001; i++){
            result = result + str;
        }
        long t2 = System.currentTimeMillis();

        //消耗时间
        System.out.println("花费的时间为:"+(t2-t1));

        //打印拼接的结果
        System.out.println("拼接的结果为:"+result);
    }

    private static void method2() {
        String str = "abcdefghijklmnopqrstuvwxyz";

        //优化: String--> StringBuilder/StringBuffer
        StringBuilder sb = new StringBuilder();
        StringBuffer sb2 = new StringBuffer();

        System.out.println(sb); // 空串
        //将指定的字符拼接10000次

        //我们可以给程序添加一个计时的功能
        long t1 = System.currentTimeMillis();

        for (int i=1; i<10001; i++){
            //result = result + str;

            /**优化: 使用append方法进行拼接*/
            //sb.append(str);
            sb2.append(str);

        }
        long t2 = System.currentTimeMillis();

        //消耗时间
        System.out.println("花费的时间为:"+(t2-t1));

        //打印拼接的结果
        System.out.println("拼接的结果为:"+sb2);
    }
}
