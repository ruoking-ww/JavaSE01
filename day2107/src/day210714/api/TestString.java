package day210714.api;

/**
 * 本类用于练习String的创建和常用方法
 *
 * 注意: 字符串底层是一个 char型数组
 *
 *  == 比较运算符 比对象时 比的是地址值
 *  Object类中equals方法默认使用的也是== 来比较
 *  但String类重写了此方法,不再比较地址值,而是两个字符串的具体内容
 *  (equals是方法 方法里面怎么定义 就怎么比)
 *
 *
 *
 */
public class TestString {
    public static void main(String[] args) {

        //练习创建String的方式一
        /**1.字符串底层维护的是char[]数组,存放在堆中*/
        char[] value = {'a','b','c'};
        String s1 = new String(value); //触发含参构造来创建String类对象
        String s11 = new String(value); //触发含参构造来创建String类对象

        //练习创建String的方式二
        /**2.此种方式创建String对象底层也会new String(),存放在堆中常量池,效率高*/
        String s2 = "abc";
        String s22 = "abc";
        String s3 = "aaa";

        System.out.println(s1 == s2);     //false
        System.out.println(s1 == s11);    //false
        System.out.println(s1 == s22);    //true
        System.out.println(s2 == s3);    //false
        System.out.println(s1.equals(s2));    //true

        int i = s1.hashCode();
        System.out.println(i);

        Object o = new Object();
        Object o1 = new Object();
        System.out.println(o.hashCode());
        System.out.println(o1.hashCode());
    }
}













