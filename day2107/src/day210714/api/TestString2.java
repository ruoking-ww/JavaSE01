package day210714.api;

import java.util.Arrays;

/**
 * 本类用于测试String类的常用方法
 *
 *
 */
public class TestString2 {
    public static void main(String[] args) {

        String s = "abc";

        char[] value = {'a','b','c','d'};
        String ss = new String(value);

        //测试常用方法
        System.out.println(s.charAt(1)); //获取指定下标处的字符

        String s1 = s.concat("xyz");
        System.out.println(s1); //abcxyz
        System.out.println(s); //abc //不会改变原字符串

        System.out.println(s.endsWith("y")); //false,判断是否以指定元素结尾

        System.out.println(s.indexOf("b")); //获取指定元素的下标值

        ss = "abcded";

        //获取指定元素最后一次出现的下标值
        System.out.println(ss.lastIndexOf("d"));
        System.out.println(ss.length()); //获取指定字符串的长度

        String s3 = "a b c d e";
        //split方法根据指定规则分割字符串,注意返回值类型是String[],需要使用数组工具类打印
        System.out.println(s3.split(" "));
        System.out.println(Arrays.toString(s3.split(" ")));

        //substring方法根据指定下标截取字符串
        //如果有两个下标,就截取这两个下标之间的字符串,注意含头不含尾

        System.out.println(String.valueOf(10));//转为String类型的"10"
        System.out.println(String.valueOf(80)+10); //8010

        Object o = new Object();
        Object o1 = new Object();
        System.out.println(o.hashCode());
        System.out.println(o1.hashCode());
    }
}
