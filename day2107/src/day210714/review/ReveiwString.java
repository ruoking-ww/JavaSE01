package day210714.review;

import java.util.Arrays;

/**
 * 本类用于复习String
 *
 *
 */
public class ReveiwString {
    public static void main(String[] args) {

        String s = "abc";

        char[] value = {'a','b','c'};

        String ss = new String(value);

        System.out.println(s.hashCode());
        System.out.println(s.toString()); //abc
        System.out.println(s.equals(ss)); //true,比较的是串的具体内容
        System.out.println(s.length());
        System.out.println(s.toUpperCase());
        System.out.println(s.toLowerCase());
        System.out.println(s.startsWith("a")); //true
        System.out.println(s.endsWith("a"));  //false
        //System.out.println(s.charAt(5)); //报错: StringIndexOutOfBoundsException: String index out of range: 5
        System.out.println(s.concat("cxy")); //abccxy
        System.out.println(s); //abc

        ss = "abcdcc";
        System.out.println(ss.indexOf("c"));
        System.out.println(ss.lastIndexOf("c"));

        ss = "  abc    ";
        System.out.println(ss.trim());

        ss = "abcdefjji";
        System.out.println(ss.substring(3));
        System.out.println(ss.substring(3, 5)); //截取,含头不含尾

        String[] es = ss.split("e");
        System.out.println(Arrays.toString(es)); //[abcd, fjji]
        String[] js = ss.split("j");
        System.out.println(Arrays.toString(js)); //[abcdef, , i]
        String[] zs = ss.split("z");
        System.out.println(Arrays.toString(zs)); //[abcdefjji]

        System.out.println("-----------------------");
        byte[] bytes = ss.getBytes(); //String转byte[]
        System.out.println(bytes);
        System.out.println(Arrays.toString(bytes));

        //---------------------------------------------------------------------
        System.out.println(String.valueOf(10)+10); //valueOf 转成字符串
        //--------------------------------------------------------------------
    }
}
