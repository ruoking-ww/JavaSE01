package day210714.aboutRegex;


import java.util.Scanner;

/**
 * 本类用于测试正则表达式
 *
 * 需求: 校验用户输入的身份证号码号是否正确
 *
 *
 */
public class TestRegex {
    public static void main(String[] args) {

        System.out.println("请输入你的身份证号:");
        String inpu = new Scanner(System.in).nextLine();

        //编辑正则表达式
        //身份证号的规律:一般都是18位,前17位是数字
        //最后一位可能是数字,也可能是X

        //String regex = "[0-9]{17}[0-9X]";

        /**单个斜杠在java中有特殊意义,会认为这是一个转义字符
         * 所以想要单纯的表示这就是一个\
         * 需要在它的前面再加一个\用来转义,也就是\\ */
        String regex = "\\d{17}[0-9X]";

        //判断用户输入的数据是否符合正则表达式的规则
        if(inpu.matches(regex)){

            System.out.println("恭喜你,输入正确");
        }else{
            System.out.println("输入有误,请检查正确后再输入");
        }

    }
}




