package day210714.aboutFile;

import org.junit.Test;

import java.io.File;
import java.io.IOException;
import java.util.Arrays;

/**
 * 本类用于测试文件类的常用方法
 *
 *
 */
public class TestFile {
    public static void main(String[] args) {

        File file = new File("F:\\Download\\1.txt");

        //测试常用方法
        System.out.println(file.length());//查看指定file的字节量
        System.out.println(file.exists());//查看指定file是否存在
        System.out.println(file.isFile());//判断指定file是否为文件
        System.out.println(file.isDirectory());
        System.out.println(file.getName());
        System.out.println(file.getParent());//获取父级路径
        System.out.println(file.getAbsoluteFile());


        //创建与删除
        file = new File("F:\\Download\\2.txt");
        try {
            System.out.println(file.createNewFile());
        } catch (IOException e) {
            e.printStackTrace();
        }

        file = new File("F:\\Download\\a");
        System.out.println(file.mkdir());//创建单层文件夹

        file = new File("F:\\Download\\a\\b\\c");
        System.out.println(file.mkdirs());//创建多层文件夹

        //测试删除
        /**注意:这个删除的方法只能删除空文件夹与文件*/
        System.out.println(file.delete()); //只有c被删除了

        file = new File("F:\\Download\\2.txt");

    }

    //文件 列表测试
    @Test
    public void test01(){
        File file = new File("F:\\Download");
        String[] listName = file.list();
        System.out.println(Arrays.toString(listName));

        /**常用listFile()方法, 因为返回值类型是 File[] 数组
         * 可以拿到一个个File对象 作进一步操作*/
        File[] files = file.listFiles();
        System.out.println(Arrays.toString(files));

        System.out.println(files[0].length());
    }
}
