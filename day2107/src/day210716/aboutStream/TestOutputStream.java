package day210716.aboutStream;

import org.junit.Test;

import java.io.*;

/**
 * 本类用于练习字节输出流
 *
 *
 */
public class TestOutputStream {

    //测试普通字节输出流
    @Test
    public void test01(){

        OutputStream out = null;

        try {
            //out = new FileOutputStream("F:\\Download\\1.txt");  //该对象write会覆盖

            /*使用两个参数的构造方法,来控制输出的数据是覆盖还是追加
            * 默认的方式是覆盖,如果想要追加,第二个参数应设置为true*/
            out = new FileOutputStream("F:\\Download\\1.txt",true);
            out.write(97); //对应ASCII码表中的a
            out.write(97);
            out.write(97);
        } catch (Exception e) {
            e.printStackTrace();
        }finally {
            try {
                if (out != null) {
                    out.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    //测试高效字节输出流
    @Test
    public void test02(){

        OutputStream out = null;

        try {

            /*使用两个参数的构造方法,来控制输出的数据是覆盖还是追加
             * 默认的方式是覆盖,如果想要追加,第二个参数应设置为true*/
            out = new BufferedOutputStream(
                    new FileOutputStream("F:\\Download\\1.txt",true));
            out.write(97); //对应ASCII码表中的a
            out.write(97);
            out.write(97);
        } catch (Exception e) {
            e.printStackTrace();
        }finally {
            try {
                if (out != null) {
                    out.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}
