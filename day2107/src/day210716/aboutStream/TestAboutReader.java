package day210716.aboutStream;

import org.junit.Test;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.io.Reader;

/**
 * 本类用于测试字符流 的读取
 *
 *
 */
public class TestAboutReader {

    //普通字符流的读取方法
    @Test
    public void test01(){

        //Reader作为字符输入流的抽象父类,不可以实例化
        //Reader r = new Reader();

        //1.创建一个在本方法中都生效的局部变量,初始值为null
        Reader in = null;
        //2.完成try-catch-finally结构
        try {
            in = new FileReader("F:\\Download\\1.txt");

            System.out.println(in);

            int b;
            while ((b = in.read()) != -1){
                System.out.println(b);
            }

        }catch (Exception e){
            e.printStackTrace();
        }finally {

            try {
                //关闭流资源,关流操作可能会发生IO异常,所以也要捕获该异常
                if (in != null) {
                    in.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }

        }
    }

    //高效字符流的读取方法
    @Test
    public void test02(){
//1.创建一个在本方法中都生效的局部变量,初始值为null
        Reader in = null;
        //2.完成try-catch-finally结构
        try {
            in = new BufferedReader(
                    new FileReader("F:\\Download\\1.txt"));

            System.out.println(in);

            int b;
            while ((b = in.read()) != -1){
                System.out.println(b);
            }

        }catch (Exception e){
            e.printStackTrace();
        }finally {

            try {
                //关闭流资源,关流操作可能会发生IO异常,所以也要捕获该异常
                if (in != null) {
                    in.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }

        }
    }
}
