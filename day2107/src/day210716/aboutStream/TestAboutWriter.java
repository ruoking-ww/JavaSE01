package day210716.aboutStream;

import org.junit.Test;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;

/**
 * 本类用于测试 字符输出流
 *
 *
 */
public class TestAboutWriter {

    //测试普通字符输出流
    @Test
    public void test01(){
        Writer out = null;

        try {
            out = new FileWriter("F:\\Download\\1.txt");

            out.write(100);
        } catch (Exception e) {
            e.printStackTrace();
        }finally {
            try {
                if (out != null) {
                    out.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    //测试高效字符输出流
    @Test
    public void test02(){
        Writer out = null;

        try {
            out = new BufferedWriter(
                    new FileWriter("F:\\Download\\1.txt",true));

            out.write(100);
            out.write(100);
            out.write(100);

            out.flush(); //把数据刷干净,防止数据遗漏. 读数据时不需要flush

        } catch (Exception e) {
            e.printStackTrace();
        }finally {
            try {
                if (out != null) {
                    out.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}
