package day210716.aboutStream;

import org.junit.Test;

import java.io.*;

/**
 * 本类用于测试字节流的读取
 *
 *
 *
 */
public class TestInputStream {

    //普通字节流的读取
    @Test
    public void test01(){

        InputStream in = null;

        //创建字节输入流对象用于读取
        //IO操作可能会发生异常,所以需要try-catch
        try {
            //InputStream in = new FileInputStream(new File("F:\\Download\\1.txt"));
            in = new FileInputStream("F:\\Download\\1.txt");

            /**read方法一次读取一个字节,如果没有数据了,返回-1*/
            //System.out.println(in.read());

            int b;
            while((b=in.read()) != -1){
                System.out.println(b);
            }

            //in.close();

        } catch (Exception e) {
            e.printStackTrace(); //打印错误信息
        }


        /*
         * finally代码块是try-catch结构中一定会被执行的部分,常用于关流操作
         */
        finally {
            try {
                //关闭流资源
                if (in != null) {
                    in.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    //高效字节流的读取
    @Test
    public void test02(){

        //定义一个在整个方法都生效的局部变量, 需要初始化为null
        BufferedInputStream bis = null;
        try {
             bis = new BufferedInputStream(
                    new FileInputStream("F:\\Download\\1.txt"));

            //使用流
            int b;
            while((b=bis.read()) != -1){
                System.out.println(b);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }finally{

            try {
                //关闭流资源
                if (bis != null) {
                    bis.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }



    }
}
