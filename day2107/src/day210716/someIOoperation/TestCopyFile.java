package day210716.someIOoperation;

import org.junit.Test;

import java.io.*;
import java.util.Scanner;

/**
 * 拓展练习-需求: 通过流来完成文件复制的操作
 *
 * 注意: 关流操作有顺序,如果有多个流,后创建的先关闭
 * 关流语句需要各自try-catch
 *
 */
public class TestCopyFile {
    public static void main(String[] args) {

        //1.提示并接收用户输入的源文件路径与目标位置
        System.out.println("请输入你要复制的源文件路径");
        String f = new Scanner(System.in).nextLine();
        System.out.println("请输入你的目标文件路径:");
        String t = new Scanner(System.in).nextLine();

        //使用字节流进行文件复制的方法
        inputStreamCopy(f,t);

        //使用字符流进行文件复制的方法
        //readerCopy(f,t);

    }

    private static void inputStreamCopy(String f, String t) {
        //1.定义在整个方法都生效的局部变量,初始化值为null
        InputStream in = null;
        OutputStream out = null;

        try {
            in = new BufferedInputStream(new FileInputStream(f));
            out = new BufferedOutputStream(new FileOutputStream(t));

            //拿到流对象以后,就可以对流对象进行操作了
            int b; //定义变量用来记录读到的数据
            while ((b = in.read()) != -1){ //循环读取源文件中的内容
                out.write(b); //将读到的内容写入到复制文件中
            }
            System.out.println("文件复制成功");

        } catch (Exception e) {
            e.printStackTrace();
            System.out.println("文件复制失败");
        }finally{
            try {
                if (out != null) {
                    out.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
            try {
                if (in != null) {
                    in.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    private static void readerCopy(String f, String t) {
        //1.定义在整个方法都生效的局部变量,初始化值为null
        Reader in = null;
        Writer out = null;

        try {
            in = new BufferedReader(new FileReader(f));
            out = new BufferedWriter(new FileWriter(t));

            //拿到流对象以后,就可以对流对象进行操作了
            int b; //定义变量用来记录读到的数据
            while ((b = in.read()) != -1){ //循环读取源文件中的内容
                out.write(b); //将读到的内容写入到复制文件中
            }
            System.out.println("文件复制成功");

        } catch (Exception e) {
            e.printStackTrace();
            System.out.println("文件复制失败");
        }finally{
            try {
                if (out != null) {
                    out.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
            try {
                if (in != null) {
                    in.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }


}
