package day210716.reviewStream;

import java.io.*;

/**
 * 本类用于复习IO的操作
 *
 *
 */
public class ReviewAboutIO {

    public static void main(String[] args) throws Exception {

        //复习流对象的创建
        //字节输入流/字符输入流
        InputStream in1 = new FileInputStream(new File(""));
        InputStream in11 = new FileInputStream("");
        InputStream in2 = new BufferedInputStream(new FileInputStream(""));

        Reader in3 = new FileReader(new File(""));
        Reader in4 = new BufferedReader(new FileReader(""));

        //字节输出流/字符输出流
        OutputStream out1 = new FileOutputStream(new File(""));
        OutputStream out2 = new BufferedOutputStream(new FileOutputStream(""));

        Writer out3 = new FileWriter(new File(""));
        Writer out4 = new BufferedWriter(new FileWriter(""));

        Class<? extends OutputStream> aClass = out1.getClass();
    }
}
