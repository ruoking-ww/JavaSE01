package day210716.encoding;

import java.io.*;

/**
 * 本类用于测试 编码转换流
 * (编码转换流不是为了把字节流转换为字符流,而是为了解决乱码问题)
 *
 * 保证 保存和打开时使用的是同一张码表,就不会出现乱码的问题
 *
 */
public class TestEncode {
    public static void main(String[] args) {
        //f1();
        f2();
    }

    private static void f2() {

        InputStreamReader in = null;

        try {

            in = new InputStreamReader(
                    new FileInputStream("1.txt"));

            char[] chars = new char[8192];
            int len = in.read(chars);
            System.out.println(new String(chars,0,len));

            System.out.println("执行成功");

        } catch (Exception e) {
            System.out.println("执行失败");
            e.printStackTrace();
        }finally{

            try {
                in.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    private static void f1() {

        OutputStreamWriter out = null;

        try {
            out = new OutputStreamWriter(
                    new FileOutputStream("1.txt"));
            //文件1.txt在项目 目录下


            out.write("换个房间号");
            System.out.println("执行成功");

        } catch (Exception e) {
            System.out.println("执行失败");
            e.printStackTrace();
        }finally{

            try {
                out.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}
