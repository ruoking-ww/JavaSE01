package day210716.serializable;

import java.io.Serializable;

/**
 *
 */

//封装一个学生类
public class Student implements Serializable {

    private static final long serialVersionUID = -2885652543873046877L;

    //定义学生的相关属性并封装
    private String name;
    private int age;
    private String addr;
    private char gender;

    public Student() {
        System.out.println("我是Student类的无参构造");
    }
    public Student(String name, int age, String addr, char gender) {
        System.out.println("我是Student类的全参构造");
        this.name = name;
        this.age = age;
        this.addr = addr;
        this.gender = gender;
    }

    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }
    public int getAge() {
        return age;
    }
    public void setAge(int age) {
        this.age = age;
    }
    public String getAddr() {
        return addr;
    }
    public void setAddr(String addr) {
        this.addr = addr;
    }
    public char getGender() {
        return gender;
    }
    public void setGender(char gender) {
        this.gender = gender;
    }

    @Override
    public String toString() {
        return "Student{" +
                "name='" + name + '\'' +
                ", age=" + age +
                ", addr='" + addr + '\'' +
                ", gender=" + gender +
                '}';
    }
}
