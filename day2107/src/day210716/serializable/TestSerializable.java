package day210716.serializable;

import java.io.*;

/**
 * 本类用于测试序列化与反序列化
 *
 * 序列化: 是指把程序中的java对象,永久保存到磁盘中,相当于写出的过程
 * 对应流的方向: out,对应的序列化流为: ObjectOutputStream
 *
 * 反序列化: 是指把之前已经序列化在文件中保存的数据,读取/恢复到程序中
 * 对应流的方向 in, 对应的反序列化流为: ObjectInputStream
 *
 *
 * 如果某个类想要完成序列化,须实现Serializable接口,否则会报错
 * java.io.NotSerializableException
 *
 * Serializable接口是一个空接口,作用是用来当做标记,标记这个类的对象可以被
 * 序列化输出
 *
 * 注意: 一个对象(A)实现了Serializable接口才可以序列化
 * 如果没有手动设置serialVersionUID,那么系统会分配一个随机的uid,
 * 该对象A可以序列化(成一个文件 X ),(文件X可以)反序列化(成一个对象A)
 * 如果对象A做了修改,那么系统会再次给对象A分配一个不同的uid,
 * 此时将文件X反序列化为对象A会报异常:java.io.InvalidClassException
 * 也就是说反序列化操作时,会先核对uid是否一致,不一致就报错
 * 我们可以手动给要序列化的对象添加一个uid,那么后期即使该对象里面的内容有所修改,
 * 反序列化操作也可成功执行,得到的对象是修改过的对象
 * ---------------------------------------------------------------------------
 * (因此我的自我理解: 反序列化操作会基于或者说会查看本地的当前对象,而不是序列化文件)
 * ----------------------------------------------------------------------------
 *
 *
 *
 *
 */
public class TestSerializable {
    public static void main(String[] args) {
        //method1(); //测试序列化
        method2(); //测试反序列化
    }

    private static void method2() {
        ObjectInputStream in = null;

        try {
            in = new ObjectInputStream(
                    new FileInputStream("F:\\Download\\1.txt"));

            Object o = in.readObject();
            System.out.println(o);
            System.out.println("序列化成功");

        } catch (Exception e) {

            System.out.println("序列化失败");
            e.printStackTrace();
        }finally{

            try {
                //关闭流资源,关流操作可能会发生IO异常,所以也要捕获该异常
                if (in != null) {
                    in.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }


    private static void method1() {

        ObjectOutputStream out = null;

        //创建流对象
        try {
             out = new ObjectOutputStream(
                    new FileOutputStream("F:\\Download\\1.txt"));

            //使用序列化流 将 Student对象序列化
            Student s = new Student(
                    "小明",22,"北京",'男');

            out.writeObject(s);
            System.out.println("序列化成功");

        } catch (Exception e) {

            System.out.println("序列化失败");
            e.printStackTrace();
        }finally{

            try {
                //关闭流资源,关流操作可能会发生IO异常,所以也要捕获该异常
                if (out != null) {
                    out.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}
