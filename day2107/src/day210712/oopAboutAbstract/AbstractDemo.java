package day210712.oopAboutAbstract;

/**
 * 本类用作 抽象类 的入门案例
 *
 * 1.被abstract修饰的方法是抽象方法,抽象方法没有方法体
 *
 * 2.被abstract修饰的类是抽象类,(抽象类不一定包含抽象方法)
 *   如果一个类包含了抽象方法,那么这个类必须被声明成一个抽象类
 *
 * 3.当一个子类继承了抽象父类以后,有两种解决方案
 *      一:变成抽象子类
 *      二:实现抽象父类中的所有抽象方法
 *
 *
 *
 */
public class AbstractDemo {

}

abstract class Car{
    public void start(){
        System.out.println("启动");
    }
    public void stop(){
        System.out.println("停止");
    }

    //创建抽象方法
    public abstract void fly();
    public abstract void fly2();
}

//abstract class BMW extends Car{
class BMW extends Car{

    @Override
    public void fly() {
        System.out.println("我的车车能飞啦");
    }

    @Override
    public void fly2() {
        System.out.println("又飞啦");
    }
}