package day210712.oopAboutAbstract;

/**
 * 本类用于测试抽象类中 构造函数的使用
 *
 * 抽象类不可以被实例化(创建对象)
 * 但抽象类有构造函数,是为了子类创建对象时调用super();
 * [抽象类也就是一个类,只是它可以拥有抽象方法]
 *
 *
 */
public class AbstractDemo2 {
}

abstract class Car2{
    public Car2(){
        System.out.println("我是car2的构造函数");
    }
}
class BMW2 extends Car2{
    public BMW2(){
        super();
        System.out.println("我是宝马类的构造函数");
    }
}







