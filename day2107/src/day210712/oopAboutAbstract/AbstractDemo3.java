package day210712.oopAboutAbstract;

/**
 * 本类用于抽象类中的 成员测试
 *
 * 1.抽象类中可以定义成员变量, 成员常量, 普通方法
 *
 * 2.抽象类不可以被实例化,如果不想让别人创建本类对象,就可以把普通类定义为抽象类
 *
 * 3.注意:类中一旦包含抽象方法,必须被声明成抽象类
 *
 *
 *
 */
public class AbstractDemo3 {

}

//创建抽象父类水果类
abstract class Fruit{
    int sum = 100;

    final String name = "小猫";

    public void clean(){
        System.out.println("水果要洗洗再吃");
    }

    public abstract void grow();
    public abstract void eat();
}

//创建子类香蕉类
class Banana extends Fruit{

    @Override
    public void grow() {
        System.out.println("大香蕉");
    }

    @Override
    public void eat() {
        System.out.println("吃香蕉");
    }
}