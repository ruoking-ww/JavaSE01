package day210712.oop;

/**
 * 本类用于回顾 多态
 *
 * 1.多态前提: 继承+重写
 * 2.父类的引用指向子类对象; 编译看左边,运行看右边
 * ---------------------------------------------
 *
 *
 *
 */
public class ReviewDemo {
    public static void main(String[] args) {

        Animal animal = new Animal();

        Cat cat = new Cat();

        Dog dog = new Dog();


    }
}

//创建父类
class Animal{
    public void eat(){
        System.out.println("吃吃吃");
    }
}
//创建子类
class Cat extends Animal{
    public void eat(){
        System.out.println("小猫吃小鱼干");
    }

    public void jump(){
        System.out.println("小猫喜欢跳");
    }
}
class Dog extends Animal{
    public void eat(){
        System.out.println("小狗吃肉骨头");
    }

    public void run(){
        System.out.println("小狗喜欢跑");
    }
}






