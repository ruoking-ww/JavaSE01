package day210712.oop;

/**
 * 本类用作final的入门案例
 *
 * 1.final表示最终
 *   可以用来修饰类,被修饰类即为最终类
 *   不可以被继承,也就是说没有子类
 *
 * 2.final可以用来修饰方法,被final修饰的方法 不可以被重写
 *
 * 3.final修饰的变量 即为 ##常量##. 常量不可以被修改+++++
 *   常量的值不可以被修改,所以在定义final常量时,
 *   必须要给它赋值,否则没有意义,会报错
 *
 *  注意: 还可以这样写
 *  class String01{
 *     private final char value[];
 *     public String01(char c[]) {
 *         this.value = c;  //此时来自外部的一次赋值,会将value值定死
 *     }
 *  }
 *
 *
 */
public class TestFinal {
}

//1.使用final修饰类
// final class Father{...}
class Father{

    final String name = "雨神";

    public void rain(){
        System.out.println("今天在下雨");

        System.out.println(name);
    }


}

class Son extends Father{

    @Override
    public void rain(){
        System.out.println("明天应该不下了");
    }
}