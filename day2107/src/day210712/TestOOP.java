package day210712;

/**
 * 本类用于测试 面向对象的综合案例
 *
 *
 *
 */
public class TestOOP {
    public static void main(String[] args) {
        Teacher cgbTeacher = new CGBTeacher();

        cgbTeacher.setAge(11);
        int age = cgbTeacher.getAge();
        System.out.println(age);
    }
}

//通过抽象一类事物的特征与行为,封装成一个类组件
abstract class Teacher{
    //属性/字段/成员变量
    private String name;
    private int age;
    private double salary;
    private String gender;

    //成员方法/普通方法/功能
    //public void ready(){
    //    System.out.println("备课");
    //}
    public abstract void teach();

    public Teacher() {
        System.out.println("我是无参构造");
    }

    public Teacher(String name, int age, double salary, String gender) {
        System.out.println("我是全参构造");
        this.name = name;
        this.age = age;
        this.salary = salary;
        this.gender = gender;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public double getSalary() {
        return salary;
    }

    public void setSalary(double salary) {
        this.salary = salary;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }
}

class CGBTeacher extends Teacher{
    @Override
    public void teach(){
        System.out.println("正在讲课电商项目");
    }
}
class ACTTeacher extends Teacher{
    @Override
    public void teach(){
        System.out.println("正在讲课框架");
    }

    public void ready(){

    }
}


