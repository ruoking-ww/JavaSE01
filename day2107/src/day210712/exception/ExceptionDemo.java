package day210712.exception;

import java.util.InputMismatchException;
import java.util.Scanner;

/**
 * 本类用作异常的 入门案例
 *
 * 输入6与0,报错: ArithmeticException 算数异常,原因是除数不能为零
 * 输入3.3,报错: InputMismatchException 输入不匹配异常
 *
 * 1.不要害怕bug
 * 2.学会看报错的提示信息
 * 3.学会看报错的行号信息,确定自己报错的位置
 *
 * 注意: 源码不会错,检查自己写的代码
 *
 * 异常类是多态特性的一个经典应用
 *
 * 异常处理方案:
 * ###方案一: try-catch (catch可以有多个,如果有多种异常类型需要分别处理的话)
 *      try{
 *          可能出现异常的代码
 *      }catch(异常类型 异常名){
 *           处理异常的代码
 *      }
 * 如果不需要特殊处理,就可以不用嵌套catch,
 * 将所有异常的子类型统一看做父类型Exception来处理,
 * 提供通用的解决方案,这也是多态最为经典的一种用法
 *
 * ###方案二: 向上抛出 -- 交给别人解决
 * 异常抛出的格式: 在方法后面写 throws+异常类型,如果有多个异常,使用逗号分隔即可
 *
 * 如果一个方法抛出了异常,那么谁调用这个方法,谁就需要处理这个异常
 * 这里的处理也有两种方案: 捕获解决和继续向上抛出
 * 但是注意: 我们一般会在调用main方法之前解决掉异常,而不是把问题抛给main方法,因为没人解决了
 *
 *
 *
 */
public class ExceptionDemo {
    public static void main(String[] args) {
        //f1();  //调用可能出现异常的方法
        //f2();
        //f3();
        method();
    }

    private static void method() {
        try {
            f3();
        } catch (Exception e) {
            System.out.println("错误,请重新输入");
        }
    }

    //创建一个抛出异常的方法
    private static void f3() throws Exception{
        System.out.println("请输入你要计算的第一个整数:");
        int a = new Scanner(System.in).nextInt();
        System.out.println("请输入你要计算的第二个整数:");
        int b = new Scanner(System.in).nextInt();
        System.out.println(a/b);
    }

    //创建一个用来 捕获处理异常的方法--自己解决
    private static void f2() {

        try {
            System.out.println("请输入你要计算的第一个整数:");
            int a = new Scanner(System.in).nextInt();
            System.out.println("请输入你要计算的第二个整数:");
            int b = new Scanner(System.in).nextInt();
            System.out.println(a/b);
        }catch (ArithmeticException e){
            System.out.println("错误:除数不能为零!");
        }catch (InputMismatchException e){
            System.out.println("错误:请输入正确的类型");
        }catch (Exception e){
            System.out.println("错误:请输入正确的数据");
        }
    }

    //创建一个 f1 方法用来暴露异常
    private static void f1() {

        System.out.println("请输入你要计算的第一个整数:");
        int a = new Scanner(System.in).nextInt();
        System.out.println("请输入你要计算的第二个整数:");
        int b = new Scanner(System.in).nextInt();
        System.out.println(a/b);
    }
}
