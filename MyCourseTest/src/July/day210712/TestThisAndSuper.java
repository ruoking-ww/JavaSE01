package July.day210712;

/**
 * 测试this和super的区别
 *
 */
public class TestThisAndSuper {

}

class AA{

    int a = 10;

    public void eat(){
        System.out.println("ads");
    }
}
class BB extends AA{

    String name;
    int age;

    public BB() {
        System.out.println("nihao");
    }

    public BB(String name, int age) {
        this();
        this.name = name;
        this.age = age;
    }

    public void bite(){
        System.out.println(this.a);
    }
}