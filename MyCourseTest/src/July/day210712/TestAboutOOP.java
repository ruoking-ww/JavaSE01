package July.day210712;

/**
 * 本类用于练习面向对象相关的 继承,多态
 *
 *
 */
public class TestAboutOOP {
    public static void main(String[] args) {

        //情况一:
        //Teacher teacher = new EnglishTeacher("paopao",29);
        //String name = teacher.getName();
        //System.out.println(name);  //输出 null

        //情况二:
        Teacher teacher = new EnglishTeacher("paopao",29);
        String name = teacher.getName();
        System.out.println(name);  //输出 paopao

        teacher.setName("李云龙");
        String name1 = teacher.getName();
        System.out.println(name1); //输出 李云龙
    }
}

abstract class Teacher{
    private String name;
    private int age;

    public Teacher() {
    }
    public Teacher(String name, int age) {
        this.name = name;
        this.age = age;
    }

    public void eat(){
        System.out.println("老师要吃饭了");
    }

    public abstract void teach();


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }
}

class EnglishTeacher extends Teacher{

    //public EnglishTeacher() {}

    /*
      因为Teacher类的空参构造器被注释掉了,也就是没有了空参构造器,
      所以作为Teacher的子类EnglishTeacher也没法拥有空参构造器,因为子类的
      空参构造器第一行[ super() ]默认需要调用父类的空参构造器
      现在子类需要一个含参构造器,其第一行
    */
    public EnglishTeacher(String name,int age){
        //情况一:如果父类有空参构造器,那么可以这样写,此时这里的name和age没有意义
        //super();

        //情况二:
        super(name,age); //这里把name和age的值传给了父类的含参构造器

    }

    @Override
    public void teach() {
        System.out.println("HelloEveryone");
    }
}













