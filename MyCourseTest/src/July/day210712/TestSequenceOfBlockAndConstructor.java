package July.day210712;

/**
 * 本类用于测试 静态代码块/构造代码块/构造方法 的 执行顺序
 *
 *
 *
 */
public class TestSequenceOfBlockAndConstructor {
    public static void main(String[] args) {
        BBB bbb = new BBB();
        /*
          执行结果:
            我是静态代码块-父类
            我是静态代码块-子类
            我是构造代码块-父类
            我是构造方法-父类
            我是构造代码块-子类
            我是构造方法-子类
         */
    }
}

class AAA{
    static{
        System.out.println("我是静态代码块-父类");
    }
    {
        System.out.println("我是构造代码块-父类");
    }
    public AAA(){
        System.out.println("我是构造方法-父类");
    }
}
class BBB extends AAA{
    static{
        System.out.println("我是静态代码块-子类");
    }
    {
        System.out.println("我是构造代码块-子类");
    }
    public BBB(){
        System.out.println("我是构造方法-子类");
    }
}