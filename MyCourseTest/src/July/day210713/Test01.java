package July.day210713;

import org.junit.Test;

/**
 * 测试 空数组 和 静态 相关 的 内容..
 *
 */
public class Test01 {

    @Test
    public void test01(){

        String[] s = new String[0];
        System.out.println(s.length);
        String[] clone = s.clone();
        System.out.println(s.equals(clone));//false
        System.out.println(s.hashCode());
        System.out.println(s); //[Ljava.lang.String;@50134894
        String s1 = new String();
        System.out.println(s1); //空

        Person[] people = new Person[0];
        System.out.println(people);  //[LJuly.day210713.Person;@2957fcb0
        Person person = new Person();
        System.out.println(person);  //July.day210713.Person@1376c05c
    }

    static{
        System.out.println("test static 1");
    }

    static{
        System.out.println("test static 2");
    }

    public static void main(String[] args) {


    }



}
class Person{
    String name;
    int age;

    static {
        System.out.println("Person中的静态代码块");
    }
    public static void test02(){
        System.out.println("你好");

    }
}