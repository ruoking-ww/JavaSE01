package August.day210804;

import org.junit.Test;

import java.util.Arrays;

public class Test1 {

    public static void main(String[] args) {
        String[] split = "boo:and:foooa".split("o");//[b, , :and:f, , , a]
        //String[] split = {"a","b","c"};
        System.out.println(Arrays.toString(split));
    }

    @Test
    public void test1(){
        String str = "长路漫漫,唯剑作伴";
        String substring = str.substring(1);
        System.out.println(substring);
    }

    @Test
    public void test2(){
        String str = "adfah";
        int i = str.lastIndexOf("a",2);
        System.out.println(i);
    }

    @Test
    public void test3(){
        int a = 10%3;
        //a++;
        //System.out.println(a);//2
        a = a++;
        System.out.println(a);//1
    }
}
